package com.ftn.teretana.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.ftn.teretana.dao.SalaDAO;
import com.ftn.teretana.model.Sala;

@Repository
public class SalaDAOImpl implements SalaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class SalaRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, Sala> sale = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			String oznaka = resultSet.getString(index++);
			int kapacitet = resultSet.getInt(index++);
			
			Sala sala = sale.get(id);
			if (sala == null) {
				sala = new Sala(id, oznaka, kapacitet);
				sale.put(sala.getId(), sala); // dodavanje u kolekciju
			}
		}
		public List<Sala> getSale() {
			return new ArrayList<>(sale.values());
		}
	}

	@Override
	public Sala findOne(Long id) {
		String sql = "SELECT * FROM sale WHERE id=?;";
		SalaRowCallBackHandler rch = new SalaRowCallBackHandler();
		jdbcTemplate.query(sql, rch, id);
		if(rch.getSale().isEmpty()) {
			return null;
		}
		return rch.getSale().get(0);
	}

	@Override
	public List<Sala> findAll() {
		String sql = "SELECT * FROM sale;";
		SalaRowCallBackHandler rch = new SalaRowCallBackHandler();
		jdbcTemplate.query(sql, rch);
		
		return rch.getSale();
	}

	@Override
	public List<Sala> findAll(String oznaka) {
		String sql = "SELECT * FROM sale WHERE oznaka LIKE ?; ";
		SalaRowCallBackHandler rch = new SalaRowCallBackHandler();
		if(oznaka!= null && oznaka !="") {
			oznaka = "%"+oznaka+"%";
		}
		else {
			oznaka = "%%";
		}
		jdbcTemplate.query(sql, rch, oznaka);
		
		return rch.getSale();
	}
	@Override
	public List<Sala> orderASC() {
		String sql = "SELECT * FROM sale ORDER BY oznaka ASC";
		SalaRowCallBackHandler rch = new SalaRowCallBackHandler();
		jdbcTemplate.query(sql, rch);

		return rch.getSale();
	}
	
	@Override
	public List<Sala> orderDESC() {
		String sql = "SELECT * FROM sale ORDER BY oznaka DESC";
		SalaRowCallBackHandler rch = new SalaRowCallBackHandler();
		jdbcTemplate.query(sql, rch);

		return rch.getSale();
	}

	@Override
	public void delete(Long id) {
		String sql = "DELETE FROM sale WHERE id = ?";
		jdbcTemplate.update(sql, id);
		
	}

	@Override
	public Sala update(Sala sala) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
		
		@Override
		public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
			String sql = "UPDATE sale SET oznaka = ?, kapacitet = ?"
					+ "	WHERE id = ?;";

			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int index = 1;
			
			preparedStatement.setString(index++, sala.getOznaka());
			preparedStatement.setInt(index++, sala.getKapacitet()); 
			preparedStatement.setLong(index++, sala.getId());

			return preparedStatement;
		}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?sala:null;
	}

	@Override
	public int save(Sala sala) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
		
		@Override
		public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
			String sql = "INSERT INTO	sale(oznaka, kapacitet)"
					+ "	VALUES(?, ?);";

			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int index = 1;
			preparedStatement.setString(index++, sala.getOznaka());
			preparedStatement.setInt(index++, sala.getKapacitet());
			return preparedStatement;
		}

	};
	GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
	boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
	return uspeh?1:0;
		
	}
}
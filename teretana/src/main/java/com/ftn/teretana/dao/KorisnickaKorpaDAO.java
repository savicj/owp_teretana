package com.ftn.teretana.dao;

import java.util.List;

import com.ftn.teretana.model.KorisnickaKorpa;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;

public interface KorisnickaKorpaDAO {


	KorisnickaKorpa findForKorisnik(Korisnik korisnik);

	int add(Long id, Long id2);

	void delete(Long korisnik, Long termin);

	void zakazi(TerminOdrzavanjaTreninga termin, Korisnik korisnik);

}

package com.ftn.teretana.dao;

import java.util.List;

import com.ftn.teretana.model.TipTreninga;

public interface TipTreningaDAO {

	List<TipTreninga> findAll();
	TipTreninga findOne(Long tipid);
	TipTreninga findByNaziv(String naziv);

}

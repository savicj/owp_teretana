package com.ftn.teretana.dao;

import java.util.List;

import com.ftn.teretana.model.Sala;

public interface SalaDAO {

	Sala findOne(Long id);
	List<Sala> findAll();
	List<Sala> findAll(String oznaka);
	List<Sala> orderDESC();
	List<Sala> orderASC();
	void delete(Long id);
	Sala update(Sala sala);
	int save(Sala sala);
}

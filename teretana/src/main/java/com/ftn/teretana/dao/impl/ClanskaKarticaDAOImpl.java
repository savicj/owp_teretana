package com.ftn.teretana.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.ftn.teretana.dao.ClanskaKarticaDAO;
import com.ftn.teretana.model.ClanskaKartica;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.service.KorisnikService;

@Repository
public class ClanskaKarticaDAOImpl implements ClanskaKarticaDAO{

	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class KarticeRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, ClanskaKartica> kartice = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			Long k = resultSet.getLong(index++);
			Korisnik korisnik = korisnikService.findOne(k);
			int poeni= resultSet.getInt(index++);
			int popust = resultSet.getInt(index++);
			boolean odobrena = resultSet.getBoolean(index++);
			
			ClanskaKartica kartica = kartice.get(id);
			if (kartica == null) {
				kartica= new ClanskaKartica(id, korisnik, popust, poeni, odobrena);
				kartice.put(kartica.getId(), kartica); // dodavanje u kolekciju
			}
		}

		public List<ClanskaKartica> getKartice() {
			return new ArrayList<>(kartice.values());
		}

	}

	@Override
	public ClanskaKartica findOne(Korisnik korisnik) {
		String sql = "SELECT * FROM kartice WHERE korisnik_id = ? ORDER BY id";
		KarticeRowCallBackHandler rch = new KarticeRowCallBackHandler();
		jdbcTemplate.query(sql, rch, korisnik.getId());
		if(rch.getKartice().isEmpty()) {
			return null;
		}
		return rch.getKartice().get(0);
	}

	@Override
	public List<ClanskaKartica> findAll() {
		String sql = "SELECT * FROM kartice ORDER BY id";
		KarticeRowCallBackHandler rch = new KarticeRowCallBackHandler();
		jdbcTemplate.query(sql, rch);
		if(rch.getKartice().isEmpty()) {
			return null;
		}
		return rch.getKartice();
	}

	@Override
	public int save(ClanskaKartica clanskaKartica) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override public PreparedStatement createPreparedStatement(Connection connection) throws SQLException { 
				String sql = " INSERT INTO kartice(korisnik_id, poeni, popust, odobrena) VALUES(?, ?, ?, ?)";
				
				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS); 
				int index = 1;
				preparedStatement.setLong(index++, clanskaKartica.getKorisnik().getId()); 
				preparedStatement.setInt(index++, clanskaKartica.getPoeni()); 					
				preparedStatement.setInt(index++, clanskaKartica.getPopust()); 					
				preparedStatement.setBoolean(index++, clanskaKartica.isOdobrena()); 					
				
				return preparedStatement; 
			}
		}; 
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder(); 
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;		
	}

	@Override
	public ClanskaKartica findOne(Long id) {
		String sql = "SELECT * FROM kartice WHERE id = ? ORDER BY id";
		KarticeRowCallBackHandler rch = new KarticeRowCallBackHandler();
		jdbcTemplate.query(sql, rch, id);
		if(rch.getKartice().isEmpty()) {
			return null;
		}
		return rch.getKartice().get(0);
	}

	@Override
	public int update(ClanskaKartica kartica) {
PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override public PreparedStatement createPreparedStatement(Connection connection) throws SQLException { 
				String sql = " UPDATE kartice SET korisnik_id= ?, poeni=?, popust=?, odobrena=? WHERE id=?";
				
				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS); 
				int index = 1;
				preparedStatement.setLong(index++, kartica.getKorisnik().getId()); 
				preparedStatement.setInt(index++, kartica.getPoeni()); 					
				preparedStatement.setInt(index++, kartica.getPopust()); 					
				preparedStatement.setBoolean(index++, kartica.isOdobrena()); 					
				preparedStatement.setLong(index++, kartica.getId()); 
				
				return preparedStatement; 
			}
		}; 
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder(); 
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
		
	}

}

package com.ftn.teretana.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.teretana.dao.TipTreningaDAO;
import com.ftn.teretana.dao.TreningDAO;
import com.ftn.teretana.model.TipTreninga;
import com.ftn.teretana.model.Trening;
import com.ftn.teretana.model.Trening.NivoTreninga;
import com.ftn.teretana.model.Trening.VrstaTreninga;

@Repository
public class TreningDAOImpl implements TreningDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private TipTreningaDAO tipTreningaDAO;

	private class TreningRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, Trening> treninzi = new LinkedHashMap<>();
		

		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			String naziv = resultSet.getString(index++);
			String treneri = resultSet.getString(index++);
			String opis = resultSet.getString(index++);
			String slika = resultSet.getString(index++);
			double cena = resultSet.getDouble(index++);
			VrstaTreninga vrstaTreninga = VrstaTreninga.valueOf(resultSet.getString(index++));
			NivoTreninga nivoTreninga = NivoTreninga.valueOf(resultSet.getString(index++));
			int trajanjeTreninga = resultSet.getInt(index++);
			double prosecnaOcena = resultSet.getDouble(index++);

			Trening trening = treninzi.get(id);
			if (trening == null) {
				trening = new Trening(id, naziv, treneri, opis, slika, cena, vrstaTreninga, nivoTreninga, trajanjeTreninga, prosecnaOcena);
				treninzi.put(trening.getId(), trening); // dodavanje u kolekciju
			}
			
			Long tipid = resultSet.getLong(index++);
			if(tipid != 0) {
				TipTreninga tip = tipTreningaDAO.findOne(tipid);
				trening.getTipTreninga().add(tip);
			}
					
		}

		public List<Trening> getTreninzi() {
			return new ArrayList<>(treninzi.values());
		}

	}

	
	@SuppressWarnings("deprecation")
	@Override
	public 	List<Trening> findAll(String naziv, TipTreninga tipTreninga, double mincena, double maxcena, String treneri, VrstaTreninga vrstaTreninga, NivoTreninga nivoTreninga) {
		
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();

		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id ";
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(naziv!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("t.naziv LIKE ?");
			imaArgumenata = true;
			naziv = "%" + naziv.strip() + "%";
			listaArgumenata.add(naziv);
		}
		
		if(tipTreninga != null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("tip.id = ?");
			imaArgumenata = true;
			listaArgumenata.add(tipTreninga.getId());
		}
		
		if(maxcena!=0) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("t.cena <= ?");
			imaArgumenata = true;
			listaArgumenata.add(maxcena);
		}
		
		if(mincena!=0) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("t.cena >= ?");
			imaArgumenata = true;
			listaArgumenata.add(mincena);
		}
		
		if(treneri!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("t.treneri LIKE ?");
			imaArgumenata = true;
			treneri = "%" + treneri.strip() + "%";
			listaArgumenata.add(treneri);
		}
		
		if(vrstaTreninga!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("t.vrsta_treninga = ?");
			imaArgumenata = true;
			listaArgumenata.add(vrstaTreninga.toString().toUpperCase());
		}
		
		if(nivoTreninga!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("t.nivo_treninga LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(nivoTreninga.toString().toUpperCase());
		}

		
		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY t.id;";
		else
			sql=sql + " ORDER BY t.id;";
				
		TreningRowCallBackHandler rch = new TreningRowCallBackHandler();
		if(listaArgumenata.isEmpty())
			jdbcTemplate.query(sql, rch);
		else
			jdbcTemplate.query(sql, listaArgumenata.toArray(), rch);
		
		return rch.getTreninzi();
	}
	
	@Override
	public List<Trening> findAll() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id ";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public Trening findOne(Long id) {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " WHERE t.id=?; ";
		TreningRowCallBackHandler rch = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rch, id);
		if(rch.getTreninzi().isEmpty()) {
			return null;
		}
		return rch.getTreninzi().get(0);
	}

	@Transactional
	@Override
	public int save(Trening trening) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena)"
						+ "	VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, trening.getNaziv());
				preparedStatement.setString(index++, trening.getTreneri());
				preparedStatement.setString(index++, trening.getOpis());
				preparedStatement.setString(index++, trening.getSlika());
				preparedStatement.setDouble(index++, trening.getCena());
				preparedStatement.setString(index++, trening.getVrstaTreninga().toString());
				preparedStatement.setString(index++, trening.getNivoTreninga().toString());
				preparedStatement.setInt(index++, trening.getTrajanjeTreninga()); 
				preparedStatement.setDouble(index++, trening.getProsecnaOcena());

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
		
	}

	@Transactional
	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM treninzi "
				+ "	WHERE id = ?;";
		
		return jdbcTemplate.update(sql, id);
	}

	@Transactional
	@Override
	public int update(Trening trening) {
			PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql= "DELETE FROM trening_tip WHERE trening_id = ?";
				jdbcTemplate.update(sql, trening.getId());
				
				sql = "INSERT INTO trening_tip(trening_id, tip_treninga_id) VALUES (?, ?);";
				for(TipTreninga tip : trening.getTipTreninga()) {
					jdbcTemplate.update(sql, trening.getId(), tip.getId());
				}
				
				sql = "UPDATE treninzi SET naziv = ?, treneri = ?, opis = ?, slika = ?, cena = ?, "
						+ "vrsta_treninga = ?, nivo_treninga = ?, trajanje_treninga = ?, prosecna_ocena = ?"
						+ "	WHERE id = ?;";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, trening.getNaziv());
				preparedStatement.setString(index++, trening.getTreneri());
				preparedStatement.setString(index++, trening.getOpis());
				preparedStatement.setString(index++, trening.getSlika());
				preparedStatement.setDouble(index++, trening.getCena());
				preparedStatement.setString(index++, trening.getVrstaTreninga().toString());
				preparedStatement.setString(index++, trening.getNivoTreninga().toString());
				preparedStatement.setInt(index++, trening.getTrajanjeTreninga()); 
				preparedStatement.setDouble(index++, trening.getProsecnaOcena());
				preparedStatement.setLong(index++, trening.getId());

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}

	@Override
	public List<Trening> orderByNazivASC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY t.naziv ASC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByNazivDESC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY t.naziv DESC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByTipTreningaASC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY tip.naziv ASC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByTipTreningaDESC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY tip.naziv DESC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByCenaASC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY t.cena ASC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByCenaDESC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY t.cena DESC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByTreneriASC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY t.treneri ASC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByTreneriDESC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY t.treneri DESC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByVrstaTreningaASC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY t.vrsta_treninga ASC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByVrstaTreningaDESC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY t.vrsta_treninga DESC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByNivoTreningaASC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY t.nivo_treninga ASC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByNivoTreningaDESC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY t.nivo_treninga DESC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByOcenaASC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY t.prosecna_ocena ASC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

	@Override
	public List<Trening> orderByOcenaDESC() {
		String sql = "SELECT t.*, tip.id tip_treninga FROM treninzi t "
				+ " LEFT JOIN trening_tip tt ON tt.trening_id = t.id"
				+ " LEFT JOIN tip_treninga tip ON tt.tip_treninga_id = tip.id "
				+ " ORDER BY t.prosecna_ocena DESC";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}

}

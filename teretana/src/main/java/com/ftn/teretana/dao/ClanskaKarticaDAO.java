package com.ftn.teretana.dao;

import java.util.List;

import com.ftn.teretana.model.ClanskaKartica;
import com.ftn.teretana.model.Korisnik;

public interface ClanskaKarticaDAO {

	ClanskaKartica findOne(Korisnik korisnik);

	List<ClanskaKartica> findAll();

	int save(ClanskaKartica clanskaKartica);

	ClanskaKartica findOne(Long id);

	int update(ClanskaKartica kartica);

}

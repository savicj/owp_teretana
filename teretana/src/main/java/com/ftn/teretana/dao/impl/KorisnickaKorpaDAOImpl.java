package com.ftn.teretana.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.ftn.teretana.dao.KorisnickaKorpaDAO;
import com.ftn.teretana.dao.KorisnikDAO;
import com.ftn.teretana.dao.TerminOdrzavanjaTreningaDAO;
import com.ftn.teretana.model.KorisnickaKorpa;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;



/*
 * 1 klasa korpa **
 * 1 tabela korpa samo korisnik i id **
 * 1 tabela korpa_termin **	
 * 
 * termin atribut list<korisnik> **
 * 1 tbl termin ** 
 * 1 tbl zakazani_termini **
 * 
 * provera slobodnih mesta termin.clanovi.size()<=termin.sala.kapacitet
 * */

@Repository
public class KorisnickaKorpaDAOImpl implements KorisnickaKorpaDAO{

	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private KorisnikDAO korisnikDAO;
	@Autowired
	private TerminOdrzavanjaTreningaDAO terminDAO;
	
	private class KorisnickaKorpaRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, KorisnickaKorpa> korpe = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			Long korisnikid = resultSet.getLong(index++);
			Korisnik korisnik = korisnikDAO.findOne(korisnikid);
			
			KorisnickaKorpa korpa = korpe.get(id);
			if (korpa == null) {
				korpa = new KorisnickaKorpa(id, korisnik);
				korpe.put(korpa.getId(), korpa); // dodavanje u kolekciju
			}
			
			Long terminid = resultSet.getLong(index++);
			if(terminid!=0) {
				TerminOdrzavanjaTreninga termin = terminDAO.findOne(terminid);
				korpa.getTreninzi().add(termin);
			}
		}

		public List<KorisnickaKorpa> getKorpe() {
			return new ArrayList<>(korpe.values());
		}
	}


	
	@Override
	public KorisnickaKorpa findForKorisnik(Korisnik korisnik) {
		  String sql = "SELECT kk.*, kt.terminid FROM korisnicka_korpa kk" +
					  " LEFT JOIN korpa_termin kt ON kt.korisnikid=kk.korisnikid" +
					  " WHERE kk.korisnikid=?;";
		  KorisnickaKorpaRowCallBackHandler rch = new KorisnickaKorpaRowCallBackHandler(); 
		  jdbcTemplate.query(sql, rch, korisnik.getId());
		  if(rch.getKorpe().isEmpty()) {
			  KorisnickaKorpa korpa = new KorisnickaKorpa();
			  korpa.setKorisnik(korisnik);
			  sql = "INSERT INTO korisnicka_korpa(korisnikid) VALUES(?);";
			  jdbcTemplate.update(sql, korisnik.getId());
			  
			  return korpa;
		  }
		  return rch.getKorpe().get(0);

	}

	@Override
	public int add(Long id, Long id2) {
		KorisnickaKorpa korpa = findForKorisnik(korisnikDAO.findOne(id));
		
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override public PreparedStatement createPreparedStatement(Connection connection) throws SQLException { 
				String sql = " INSERT INTO korpa_termin(korisnikid, terminid) VALUES(?, ?)";
				if(korpa==null) {
					sql =	" INSERT INTO korpa_termin(korisnikid, terminid) VALUES(?, ?);"
					 +"INSERT INTO korisnicka_korpa(korisnikid) VALUES(?);";

				}
				
				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS); 
				int index = 1;
				preparedStatement.setLong(index++, id); 
				preparedStatement.setLong(index++, id2); 					
				if(korpa==null) {
					preparedStatement.setLong(index++, id); 
					preparedStatement.setLong(index++, id2); 					
					preparedStatement.setLong(index++, id); 
				}
				return preparedStatement; 
			}
		}; 
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder(); 
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;		
	}

	@Override
	public void delete(Long korisnik, Long termin) {
				
		String sql = "DELETE FROM korpa_termin " +
				 	" WHERE korisnikid = ? AND terminid = ?;";
		jdbcTemplate.update(sql, korisnik, termin);
		Korisnik k = korisnikDAO.findOne(korisnik);
		KorisnickaKorpa korpa = findForKorisnik(k);
		if(korpa.getTreninzi() == null || korpa.getTreninzi().isEmpty()) {
			sql = "DELETE FROM korisnicka_korpa WHERE korisnikid=?";
			jdbcTemplate.update(sql, korisnik);
		}		
	}


	@Override
	public void zakazi(TerminOdrzavanjaTreninga termin, Korisnik korisnik) {
		String sql = "INSERT INTO zakazani_termini(terminid, korisnikid) VALUES (?,?);";
		jdbcTemplate.update(sql, termin.getId(), korisnik.getId());
	}

}

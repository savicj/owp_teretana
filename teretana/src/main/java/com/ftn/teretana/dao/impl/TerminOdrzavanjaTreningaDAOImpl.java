package com.ftn.teretana.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.ftn.teretana.dao.KorisnikDAO;
import com.ftn.teretana.dao.SalaDAO;
import com.ftn.teretana.dao.TerminOdrzavanjaTreningaDAO;
import com.ftn.teretana.dao.TreningDAO;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.Sala;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;
import com.ftn.teretana.model.Trening;

@Repository
public class TerminOdrzavanjaTreningaDAOImpl implements TerminOdrzavanjaTreningaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private SalaDAO salaDAO;
	@Autowired 
	private TreningDAO treningDAO;
	@Autowired 
	private KorisnikDAO korisnikDAO;
	
	private class TerminRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, TerminOdrzavanjaTreninga> termini = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			Long salaid = resultSet.getLong(index++);
			Sala sala = salaDAO.findOne(salaid);
			Long treningid = resultSet.getLong(index++);
			Trening trening = treningDAO.findOne(treningid);
			LocalDateTime vremeOdrzavanja = resultSet.getTimestamp(index++).toLocalDateTime();

			TerminOdrzavanjaTreninga termin = termini.get(id);
			if (termin == null) {
				termin = new TerminOdrzavanjaTreninga(id, sala, trening, vremeOdrzavanja);
				termin.setClanovi(new ArrayList<Korisnik>());
				termini.put(termin.getId(), termin); // dodavanje u kolekciju
			}
			
			Long clanid = resultSet.getLong(index++);
			Korisnik clan = korisnikDAO.findOne(clanid);
			termin.getClanovi().add(clan);
		}

		public List<TerminOdrzavanjaTreninga> getTermini() {
			return new ArrayList<>(termini.values());
		}

	}

	@Override
	public List<TerminOdrzavanjaTreninga> findAll() {
		String sql = "SELECT * FROM termini_odrzavanja_treninga tot"
				+ " LEFT JOIN zakazani_termini z ON z.terminid=tot.id "
				+ ";";
		TerminRowCallBackHandler rch = new TerminRowCallBackHandler();
		jdbcTemplate.query(sql, rch);
		
		return rch.getTermini();
	}

	@Override
	public List<TerminOdrzavanjaTreninga> findForDatum(LocalDate datum) {
		String sql ="SELECT tot.*, k.id FROM termini_odrzavanja_treninga tot "
				+ " LEFT JOIN zakazani_termini z ON z.terminid = tot.id "
				+ " LEFT JOIN korisnici k ON k.id = z.korisnikid"
				+ " WHERE tot.datum = ?;"; 
		TerminRowCallBackHandler rch = new TerminRowCallBackHandler();
		jdbcTemplate.query(sql, rch, datum);
		if(rch.getTermini().isEmpty()) {
			return null;
		}
		return rch.getTermini();
	}

	@Override
	public List<TerminOdrzavanjaTreninga> findForTrening(Long trening) {
		String sql = "SELECT tot.*, k.id FROM termini_odrzavanja_treninga tot "
				+ " LEFT JOIN zakazani_termini z ON z.terminid = tot.id "
				+ " LEFT JOIN korisnici k ON k.id = z.korisnikid"
				+ " WHERE tot.trening_id = ?;";
		TerminRowCallBackHandler rch = new TerminRowCallBackHandler();
		jdbcTemplate.query(sql, rch, trening);
		if(rch.getTermini().isEmpty()) {
			return null;
		}
		return rch.getTermini();
	}

	@Override
	public TerminOdrzavanjaTreninga findOne(Long id) {
		String sql ="SELECT tot.*, k.id FROM termini_odrzavanja_treninga tot "
				+ " LEFT JOIN zakazani_termini z ON z.terminid = tot.id "
				+ " LEFT JOIN korisnici k ON k.id = z.korisnikid"
				+ " WHERE tot.id = ?;"; 
		TerminRowCallBackHandler rch = new TerminRowCallBackHandler();
		jdbcTemplate.query(sql, rch, id);
		if(rch.getTermini().isEmpty()) {
			return null;
		}
		return rch.getTermini().get(0);
	}

	@Override
	public Sala pronadjiSalu(Long id) {
		String sql = "SELECT tot.*, k.id FROM termini_odrzavanja_treninga tot "
				+ " LEFT JOIN zakazani_termini z ON z.terminid = tot.id "
				+ " LEFT JOIN korisnici k ON k.id = z.korisnikid"
				+ " WHERE tot.id = ?;";
		TerminRowCallBackHandler rch = new TerminRowCallBackHandler();
		jdbcTemplate.query(sql, rch, id);
		if(rch.getTermini().isEmpty()) {
			System.out.println("nema termina sa trazenim id-em");
		}
		TerminOdrzavanjaTreninga termini = rch.getTermini().get(0);
		Sala sala = termini.getSala();		
		
		return sala;
	}

	@Override
	public List<TerminOdrzavanjaTreninga> findForSala(Long id) {
		String sql ="SELECT tot.*, k.id FROM termini_odrzavanja_treninga tot "
				+ " LEFT JOIN zakazani_termini z ON z.terminid = tot.id "
				+ " LEFT JOIN korisnici k ON k.id = z.korisnikid"
				+ " WHERE tot.sala_id = ?;"; 
		TerminRowCallBackHandler rch = new TerminRowCallBackHandler();
		jdbcTemplate.query(sql, rch, id);
		if(rch.getTermini().isEmpty()) {
			return null;
		}
		return rch.getTermini();
	}

	@Override
	public List<TerminOdrzavanjaTreninga> findForKorisnik(Long id) {
		String sql ="SELECT tot.*, k.id FROM termini_odrzavanja_treninga tot "
				+ " LEFT JOIN zakazani_termini z ON z.terminid = tot.id "
				+ " LEFT JOIN korisnici k ON k.id = z.korisnikid"
				+ " WHERE k.id = ?"
				+ "	ORDER BY tot.vreme DESC;"; 
		TerminRowCallBackHandler rch = new TerminRowCallBackHandler();
		jdbcTemplate.query(sql, rch, id);
		
		return rch.getTermini();
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<TerminOdrzavanjaTreninga> preklapanjeTermina(TerminOdrzavanjaTreninga termin, Korisnik korisnik) {
		String sql = "select termini_odrzavanja_treninga.*, k.id from termini_odrzavanja_treninga "
				+ "left join zakazani_termini zt on zt.terminid= termini_odrzavanja_treninga.id "
				+ "left join korisnici k on k.id = zt.korisnikid  "
				+ "left join treninzi t on termini_odrzavanja_treninga.trening_id = t.id "
				+ "where "
				+ " (? between termini_odrzavanja_treninga.vreme and date_add(termini_odrzavanja_treninga.vreme, interval t.trajanje_treninga minute)) and k.id=? "
				+ " or  (date_add(?, interval ? minute) between termini_odrzavanja_treninga.vreme and date_add(termini_odrzavanja_treninga.vreme, interval t.trajanje_treninga minute)) and k.id=?"
				+ " or (termini_odrzavanja_treninga.vreme between ? and date_add(?, interval ? minute)) and k.id=?;";
		TerminRowCallBackHandler rch = new TerminRowCallBackHandler();
		List<Object> args = new ArrayList<Object>();
		args.add(termin.getVremeOdrzavanja());
		args.add(korisnik.getId());
		args.add(termin.getVremeOdrzavanja());
		args.add(termin.getTrening().getTrajanjeTreninga());
		args.add(korisnik.getId());
		args.add(termin.getVremeOdrzavanja());
		args.add(termin.getVremeOdrzavanja());
		args.add(termin.getTrening().getTrajanjeTreninga());
		args.add(korisnik.getId());
		jdbcTemplate.query(sql, args.toArray(), rch);
		
		return rch.getTermini();
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<TerminOdrzavanjaTreninga> preklapanjeTermina(TerminOdrzavanjaTreninga termin) {
		String sql = "select termini_odrzavanja_treninga.*, zt.korisnikid from termini_odrzavanja_treninga "
				+ "left join zakazani_termini zt on zt.terminid= termini_odrzavanja_treninga.id "
				+ "left join sale on sale.id = termini_odrzavanja_treninga.sala_id "
				+ "left join treninzi t on termini_odrzavanja_treninga.trening_id = t.id "
				+ "where "
				+ " (? between termini_odrzavanja_treninga.vreme and date_add(termini_odrzavanja_treninga.vreme, interval t.trajanje_treninga minute)) and sale.id=?"
				+ " or  (date_add(?, interval ? minute) between termini_odrzavanja_treninga.vreme and date_add(termini_odrzavanja_treninga.vreme, interval t.trajanje_treninga minute)) and sale.id=? "
				+ " or (termini_odrzavanja_treninga.vreme between ? and date_add(?, interval ? minute)) and sale.id=?;";
		TerminRowCallBackHandler rch = new TerminRowCallBackHandler();
		List<Object> args = new ArrayList<Object>();
		args.add(termin.getVremeOdrzavanja());
		args.add(termin.getSala().getId());
		args.add(termin.getVremeOdrzavanja());
		args.add(termin.getTrening().getTrajanjeTreninga());
		args.add(termin.getSala().getId());
		args.add(termin.getVremeOdrzavanja());
		args.add(termin.getVremeOdrzavanja());
		args.add(termin.getTrening().getTrajanjeTreninga());
		args.add(termin.getSala().getId());
		jdbcTemplate.query(sql, args.toArray(), rch);
		
		return rch.getTermini();
	}

	@Override
	public int save(TerminOdrzavanjaTreninga termin) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO	termini_odrzavanja_treninga(sala_id, trening_id, vreme)"
						+ "	VALUES(?, ?, ?);";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setLong(index++, termin.getSala().getId());
				preparedStatement.setLong(index++, termin.getTrening().getId());
				preparedStatement.setObject(index++, termin.getVremeOdrzavanja());
				

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;		
	}
}

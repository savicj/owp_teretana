package com.ftn.teretana.dao;

import java.util.List;

import com.ftn.teretana.model.Korisnik;

public interface KorisnikDAO {

	Korisnik findOne(String email, String sifra);

	int save(Korisnik korisnik);

	List<Korisnik> findAll(String korisnickoIme, String uloga);

	Korisnik findOne(Long id);

	int update(Korisnik izmenjen);

	void block(Long id);

	List<Korisnik> orderByKorisnickoImeASC();

	List<Korisnik> orderByUlogaDESC();

	List<Korisnik> orderByUlogaASC();

	List<Korisnik> orderByKorisnickoImeDESC();

}

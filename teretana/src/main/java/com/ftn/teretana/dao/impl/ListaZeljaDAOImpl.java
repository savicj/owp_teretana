package com.ftn.teretana.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.ftn.teretana.dao.KorisnikDAO;
import com.ftn.teretana.dao.ListaZeljaDAO;
import com.ftn.teretana.dao.TerminOdrzavanjaTreningaDAO;
import com.ftn.teretana.model.KorisnickaKorpa;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.ListaZelja;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;

@Repository
public class ListaZeljaDAOImpl implements ListaZeljaDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private KorisnikDAO korisnikDAO;
	@Autowired
	private TerminOdrzavanjaTreningaDAO terminDAO;
	
	private class ListaZeljaRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, ListaZelja> listaZelja = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			Long korisnikid = resultSet.getLong(index++);
			Korisnik korisnik = korisnikDAO.findOne(korisnikid);
			
			ListaZelja zelja = listaZelja.get(id);
			if (zelja == null) {
				zelja = new ListaZelja(id, korisnik);
				
				listaZelja.put(zelja.getId(), zelja); // dodavanje u kolekciju
			}
			
			Long terminid = resultSet.getLong(index++);
			TerminOdrzavanjaTreninga trening = terminDAO.findOne(terminid);
			System.err.println(terminid);

			zelja.getTermin().add(trening);
		}

		public List<ListaZelja> getZelje() {
			return new ArrayList<>(listaZelja.values());
		}

	}

	@Override
	public List<TerminOdrzavanjaTreninga> findForKorisnik(Long id) {
		String sql = "SELECT lz.*, z.termin_id FROM lista_zelja lz"
				+ " LEFT JOIN zelje z ON z.korisnik_id=lz.korisnik_id "
				+ " WHERE lz.korisnik_id=?;";
		ListaZeljaRowCallBackHandler rch = new ListaZeljaRowCallBackHandler();
		jdbcTemplate.query(sql, rch, id);

		if(rch.getZelje()!=null && !rch.getZelje().isEmpty())
			return rch.getZelje().get(0).getTermin();
		else 
			return new ArrayList<TerminOdrzavanjaTreninga>();
	}

	@Override
	public int delete(Long terminid, Long korisnikid) {
		String sql = "DELETE FROM zelje WHERE korisnik_id = ? AND termin_id = ?";
		return jdbcTemplate.update(sql, korisnikid, terminid);
	}

	@Override
	public int add(Long korisnikid, Long terminid) {		
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override public PreparedStatement createPreparedStatement(Connection connection) throws SQLException { 
				String sql = " INSERT INTO zelje(korisnik_id, termin_id) VALUES(?, ?)";
				
				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS); 
				int index = 1;
				preparedStatement.setLong(index++, korisnikid); 
				preparedStatement.setLong(index++, terminid); 					
				
				return preparedStatement; 
			}
		}; 
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder(); 
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;		
	}


}

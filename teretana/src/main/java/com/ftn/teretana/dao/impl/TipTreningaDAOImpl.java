package com.ftn.teretana.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.ftn.teretana.dao.TipTreningaDAO;
import com.ftn.teretana.model.TipTreninga;

@Repository
public class TipTreningaDAOImpl implements TipTreningaDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class AutorRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, TipTreninga> tipovi = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			String naziv = resultSet.getString(index++);
			String opis = resultSet.getString(index++);

			TipTreninga tip = tipovi.get(id);
			if (tip == null) {
				tip = new TipTreninga(id, naziv, opis);
				tipovi.put(tip.getId(), tip); // dodavanje u kolekciju
			}
		}

		public List<TipTreninga> getTipovi() {
			return new ArrayList<>(tipovi.values());
		}

	}
	@Override
	public TipTreninga findOne(Long id) {
		String sql = 
				"SELECT * FROM tip_treninga  " + 
				"WHERE id = ? " + 
				"ORDER BY id";

		AutorRowCallBackHandler rowCallbackHandler = new AutorRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getTipovi().get(0);
	}
	@Override
	public TipTreninga findByNaziv(String naziv) {
		String sql = 
				"SELECT * FROM tip_treninga  " + 
				"WHERE naziv = ? " + 
				"ORDER BY id";

		AutorRowCallBackHandler rowCallbackHandler = new AutorRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, naziv);

		if(rowCallbackHandler.getTipovi().isEmpty())
			return null;
		else					
			return rowCallbackHandler.getTipovi().get(0);
	}
	@Override
	public List<TipTreninga> findAll() {
		String sql = 
				"SELECT * FROM tip_treninga  " + 
				"ORDER BY id";

		AutorRowCallBackHandler rowCallbackHandler = new AutorRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTipovi();
	}
	

}

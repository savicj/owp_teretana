package com.ftn.teretana.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.teretana.dao.KorisnikDAO;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.Korisnik.Uloga;

@Repository
public class KorisnikDAOImpl implements KorisnikDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class KorisnikRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, Korisnik> korisnici = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			String korisnickoIme = resultSet.getString(index++);
			String lozinka = resultSet.getString(index++);
			String email = resultSet.getString(index++);
			String ime = resultSet.getString(index++);
			String prezime = resultSet.getString(index++);
			LocalDate datumRodjenja = resultSet.getTimestamp(index++).toLocalDateTime().toLocalDate();
			String adresa = resultSet.getString(index++);
			String brojTelefona = resultSet.getString(index++);
			LocalDateTime datumRegistracije = resultSet.getTimestamp(index++).toLocalDateTime();
			String u = resultSet.getString(index++);
			Uloga uloga = Uloga.valueOf(u);
			boolean blokiran = resultSet.getBoolean(index++);
			
			Korisnik korisnik = korisnici.get(id);
			if (korisnik == null) {
				korisnik = new Korisnik(id, korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumRegistracije, uloga, blokiran);
				korisnici.put(korisnik.getId(), korisnik); // dodavanje u kolekciju
			}
		}

		public List<Korisnik> getKorisnici() {
			return new ArrayList<>(korisnici.values());
		}

	}

	@Override
	public Korisnik findOne(String email, String sifra) {
		String sql = "SELECT * FROM korisnici WHERE korisnicko_ime=? AND lozinka=? AND blokiran = false ORDER BY id";
		KorisnikRowCallBackHandler rch = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rch, email, sifra);
		if(rch.getKorisnici().isEmpty()) {
			return null;
		}
		return rch.getKorisnici().get(0);
	}

	@Transactional
	@Override
	public int save(Korisnik korisnik) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO korisnici(korisnicko_ime, lozinka, email, ime, prezime, datum_rodjenja, adresa, telefon, datum_vreme_registracije, uloga, blokiran) "
						+ "	VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, false);";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, korisnik.getKorisnickoIme());
				preparedStatement.setString(index++, korisnik.getLozinka());
				preparedStatement.setString(index++, korisnik.getEmail());
				preparedStatement.setString(index++, korisnik.getIme());
				preparedStatement.setString(index++, korisnik.getPrezime());
				preparedStatement.setObject(index++, korisnik.getDatumRodjenja());
				preparedStatement.setString(index++, korisnik.getAdresa());
				preparedStatement.setString(index++, korisnik.getBrojTelefona());
				preparedStatement.setObject(index++, korisnik.getDatumIVremeRegistracije()); //setTimestamp(index++, Timestamp.of(korisnik.getDatumIVremeRegistracije));
				preparedStatement.setString(index++, korisnik.getUloga().toString());

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Korisnik> findAll(String korisnickoIme, String uloga) {
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		boolean imaArgumenata = false;
		StringBuffer whereSql = new StringBuffer(" ");
		
		String sql = "SELECT * FROM korisnici WHERE blokiran = false ";
		
		if(korisnickoIme != null && korisnickoIme != "") {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("AND korisnicko_ime LIKE ? ");
			imaArgumenata = true;
			korisnickoIme = "%" + korisnickoIme + "%";
			listaArgumenata.add(korisnickoIme);
		}
		if(uloga != null && uloga != "") {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append(" AND uloga LIKE ? ");
			imaArgumenata = true;
			listaArgumenata.add(uloga.toUpperCase());
		}
		
		if(imaArgumenata)
			sql = sql + whereSql.toString()+ " ORDER BY id;";
		else
			sql=sql + " ORDER BY id;";
				
		KorisnikRowCallBackHandler rch = new KorisnikRowCallBackHandler();
		if(listaArgumenata.isEmpty())
			jdbcTemplate.query(sql, rch);
		else
			jdbcTemplate.query(sql, listaArgumenata.toArray(), rch);
		
		return rch.getKorisnici();
	}

	@Override
	public Korisnik findOne(Long id) {
		String sql = "SELECT * FROM korisnici WHERE id=? AND blokiran = false ORDER BY id";
		KorisnikRowCallBackHandler rch = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rch, id);
		if(rch.getKorisnici().isEmpty()) {
			return null;
		}
		return rch.getKorisnici().get(0);
	}

	@Transactional
	@Override
	public int update(Korisnik izmenjen) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
		@Override
		public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
			
			String sql = "UPDATE korisnici SET korisnicko_ime = ?, lozinka = ?, email = ?, ime = ?, prezime = ?, "
					+ "datum_rodjenja = ?, adresa = ?, telefon = ?, datum_vreme_registracije = ?, uloga = ?, blokiran = ?"
					+ "	WHERE id = ?;";

			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int index = 1;
			preparedStatement.setString(index++, izmenjen.getKorisnickoIme());
			preparedStatement.setString(index++, izmenjen.getLozinka());
			preparedStatement.setString(index++, izmenjen.getEmail());
			preparedStatement.setString(index++, izmenjen.getIme());
			preparedStatement.setString(index++, izmenjen.getPrezime());
			preparedStatement.setObject(index++, izmenjen.getDatumRodjenja());
			preparedStatement.setString(index++, izmenjen.getAdresa());
			preparedStatement.setString(index++, izmenjen.getBrojTelefona()); 
			preparedStatement.setObject(index++, izmenjen.getDatumIVremeRegistracije());
			preparedStatement.setString(index++, izmenjen.getUloga().toString()); 
			preparedStatement.setBoolean(index++, izmenjen.isBlokiran()); 
			preparedStatement.setLong(index++, izmenjen.getId());

			return preparedStatement;
		}

	};
	GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
	boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
	return uspeh?1:0;
	}

	@Override
	public void block(Long id) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				
				String sql = "UPDATE korisnici SET blokiran = ?"
						+ "	WHERE id = ?;";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				
				preparedStatement.setBoolean(index++, true); 
				preparedStatement.setLong(index++, id);

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(preparedStatementCreator, keyHolder);
		
		
	}

	@Override
	public List<Korisnik> orderByKorisnickoImeASC() {
		String sql = "SELECT * FROM korisnici WHERE blokiran = false ORDER BY korisnicko_ime ASC";
		KorisnikRowCallBackHandler rch = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rch);

		return rch.getKorisnici();
	}
		
	@Override
	public List<Korisnik> orderByKorisnickoImeDESC() {
		String sql = "SELECT * FROM korisnici WHERE blokiran = false ORDER BY korisnicko_ime DESC";
		KorisnikRowCallBackHandler rch = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rch);

		return rch.getKorisnici();
	}
	
	@Override
	public List<Korisnik> orderByUlogaASC() {
		String sql = "SELECT * FROM korisnici WHERE blokiran = false ORDER BY uloga ASC";
		KorisnikRowCallBackHandler rch = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rch);

		return rch.getKorisnici();
	}
	
	@Override
	public List<Korisnik> orderByUlogaDESC() {
		String sql = "SELECT * FROM korisnici WHERE blokiran = false ORDER BY uloga DESC";
		KorisnikRowCallBackHandler rch = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rch);

		return rch.getKorisnici();
	}
}

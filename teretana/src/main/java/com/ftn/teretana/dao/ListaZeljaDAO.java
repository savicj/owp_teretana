package com.ftn.teretana.dao;

import java.util.List;

import com.ftn.teretana.model.TerminOdrzavanjaTreninga;

public interface ListaZeljaDAO {

	List<TerminOdrzavanjaTreninga> findForKorisnik(Long id);

	int delete(Long terminid, Long korisnikid);

	int add(Long korisnikid, Long terminid);

}

package com.ftn.teretana.dao;

import java.time.LocalDate;
import java.util.List;

import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.Sala;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;

public interface TerminOdrzavanjaTreningaDAO {

	List<TerminOdrzavanjaTreninga> findAll();
	List<TerminOdrzavanjaTreninga> findForDatum(LocalDate datum);
	List<TerminOdrzavanjaTreninga> findForTrening(Long trening);
	TerminOdrzavanjaTreninga findOne(Long id);
	Sala pronadjiSalu(Long id);
	List<TerminOdrzavanjaTreninga> findForSala(Long id);
	List<TerminOdrzavanjaTreninga> findForKorisnik(Long id);
	List<TerminOdrzavanjaTreninga> preklapanjeTermina(TerminOdrzavanjaTreninga termin, Korisnik korisnik);
	List<TerminOdrzavanjaTreninga> preklapanjeTermina(TerminOdrzavanjaTreninga termin);
	int save(TerminOdrzavanjaTreninga termin);

}

package com.ftn.teretana.model;

import java.time.LocalDateTime;

public class Komentar {

	public enum StatusKomentara {
		NA_CEKANJU,	ODOBREN, NIJE_ODOBREN
}
	
	private Long id;
	private String tekst;
	private int ocena;
	private LocalDateTime datumPostavljanjaKomentara;
	private Korisnik autor;
	private Trening trening;
	private StatusKomentara status;
	private boolean anoniman;
	
	public Komentar(String tekst, int ocena, LocalDateTime datumPostavljanjaKomentara, Korisnik autor, Trening trening,
			StatusKomentara status, boolean anoniman) {
		super();
		this.tekst = tekst;
		this.ocena= ocena; 
		this.datumPostavljanjaKomentara = datumPostavljanjaKomentara;
		this.autor = autor;
		this.trening = trening;
		this.status = status;
		this.anoniman = anoniman;
	}
	
	public Komentar(Long id, String tekst, int ocena, LocalDateTime datumPostavljanjaKomentara, Korisnik autor, Trening trening,
			StatusKomentara status, boolean anoniman) {
		super();
		this.id = id;
		this.tekst = tekst;
		this.ocena = ocena;
		this.datumPostavljanjaKomentara = datumPostavljanjaKomentara;
		this.autor = autor;
		this.trening = trening;
		this.status = status;
		this.anoniman = anoniman;
	}
	
	public Komentar() {
		super();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTekst() {
		return tekst;
	}
	public void setTekst(String tekst) {
		this.tekst = tekst;
	}
	public int getOcena() {
		return ocena;
	}

	public void setOcena(int ocena) {
		this.ocena = ocena;
	}

	public LocalDateTime getDatumPostavljanjaKomentara() {
		return datumPostavljanjaKomentara;
	}
	public void setDatumPostavljanjaKomentara(LocalDateTime datumPostavljanjaKomentara) {
		this.datumPostavljanjaKomentara = datumPostavljanjaKomentara;
	}
	public Korisnik getAutor() {
		return autor;
	}
	public void setAutor(Korisnik autor) {
		this.autor = autor;
	}
	public Trening getTrening() {
		return trening;
	}
	public void setTrening(Trening trening) {
		this.trening = trening;
	}
	public StatusKomentara getStatus() {
		return status;
	}
	public void setStatus(StatusKomentara status) {
		this.status = status;
	}
	public boolean isAnoniman() {
		return anoniman;
	}
	public void setAnoniman(boolean anoniman) {
		this.anoniman = anoniman;
	}

	@Override
	public String toString() {
		return id + ", " + tekst + ", " + datumPostavljanjaKomentara + ", " + autor + ", " + trening + ", " + status
				+ ", " + anoniman;
	}
	
	
	
	
}

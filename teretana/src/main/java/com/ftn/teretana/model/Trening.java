package com.ftn.teretana.model;

import java.util.ArrayList;
import java.util.List;

public class Trening {

	public enum VrstaTreninga{
		POJEDINACNI, GRUPNI
	}
	public enum NivoTreninga{
		AMATERSKI, SREDNJI, NAPREDNI
	}
	
	private Long id;
	private String naziv;
	private String treneri;
	private String opis;
	private String slika;
	private List<TipTreninga> tipTreninga = new ArrayList<TipTreninga>();
	private double cena;
	private VrstaTreninga vrstaTreninga;
	private NivoTreninga nivoTreninga;
	private int trajanjeTreninga;
	private double prosecnaOcena; // generise aplikacija
	public Trening(Long id, String naziv, String treneri, String opis, String slika, List<TipTreninga> tipTreninga,
			double cena, VrstaTreninga vrstaTreninga, NivoTreninga nivoTreninga, int trajanjeTreninga,
			double prosecnaOcena) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.treneri = treneri;
		this.opis = opis;
		this.slika = slika;
		this.tipTreninga = tipTreninga;
		this.cena = cena;
		this.vrstaTreninga = vrstaTreninga;
		this.nivoTreninga = nivoTreninga;
		this.trajanjeTreninga = trajanjeTreninga;
		this.prosecnaOcena = prosecnaOcena;
	}
	public Trening(Long id, String naziv, String treneri, String opis, String slika,
			double cena, VrstaTreninga vrstaTreninga, NivoTreninga nivoTreninga, int trajanjeTreninga,
			double prosecnaOcena) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.treneri = treneri;
		this.opis = opis;
		this.slika = slika;
		this.cena = cena;
		this.vrstaTreninga = vrstaTreninga;
		this.nivoTreninga = nivoTreninga;
		this.trajanjeTreninga = trajanjeTreninga;
		this.prosecnaOcena = prosecnaOcena;
	}
	public Trening(String naziv, String treneri, String opis, String slika, List<TipTreninga> tipTreninga, double cena,
			VrstaTreninga vrstaTreninga, NivoTreninga nivoTreninga, int trajanjeTreninga, double prosecnaOcena) {
		super();
		this.naziv = naziv;
		this.treneri = treneri;
		this.opis = opis;
		this.slika = slika;
		this.tipTreninga = tipTreninga;
		this.cena = cena;
		this.vrstaTreninga = vrstaTreninga;
		this.nivoTreninga = nivoTreninga;
		this.trajanjeTreninga = trajanjeTreninga;
		this.prosecnaOcena = prosecnaOcena;
	}
	public Trening(String naziv, String treneri, String opis, String slika, double cena,
			VrstaTreninga vrstaTreninga, NivoTreninga nivoTreninga, int trajanjeTreninga, double prosecnaOcena) {
		super();
		this.naziv = naziv;
		this.treneri = treneri;
		this.opis = opis;
		this.slika = slika;
		this.cena = cena;
		this.vrstaTreninga = vrstaTreninga;
		this.nivoTreninga = nivoTreninga;
		this.trajanjeTreninga = trajanjeTreninga;
		this.prosecnaOcena = prosecnaOcena;
	}
	public Trening() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getTreneri() {
		return treneri;
	}
	public void setTreneri(String treneri) {
		this.treneri = treneri;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getSlika() {
		return slika;
	}
	public void setSlika(String slika) {
		this.slika = slika;
	}
	public List<TipTreninga> getTipTreninga() {
		return tipTreninga;
	}
	public void setTipTreninga(List<TipTreninga> tipTreninga) {
		this.tipTreninga = tipTreninga;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public VrstaTreninga getVrstaTreninga() {
		return vrstaTreninga;
	}
	public void setVrstaTreninga(VrstaTreninga vrstaTreninga) {
		this.vrstaTreninga = vrstaTreninga;
	}
	public NivoTreninga getNivoTreninga() {
		return nivoTreninga;
	}
	public void setNivoTreninga(NivoTreninga nivoTreninga) {
		this.nivoTreninga = nivoTreninga;
	}
	public int getTrajanjeTreninga() {
		return trajanjeTreninga;
	}
	public void setTrajanjeTreninga(int trajanjeTreninga) {
		this.trajanjeTreninga = trajanjeTreninga;
	}
	public double getProsecnaOcena() {
		return prosecnaOcena;
	}
	public void setProsecnaOcena(double prosecnaOcena) {
		this.prosecnaOcena = prosecnaOcena;
		//List<Komentar> komentari = komentariService.getByTrening(this.id);
		//int ocena = 0;
		// for(int i = 0; i<komentari.length(); i++){
		//		ocena += komentari[i].getOcena();
		//}
		//prosecnaOcena = ocena/komentari.length();
	}
	@Override
	public String toString() {
		return id + ", " + naziv + ", " + treneri + ", " + opis + ", " + slika + ", " + tipTreninga + ", " + cena + ", "
				+ vrstaTreninga + ", " + nivoTreninga + ", " + trajanjeTreninga + ", " + prosecnaOcena;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null || obj.getClass() != Trening.class) {
	        return false;
	    }
		Trening other = (Trening) obj;
		if(other.id != this.id)
			return false;
		
		return true;
	}
	
	
}

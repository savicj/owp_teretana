package com.ftn.teretana.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TerminOdrzavanjaTreninga {
//sala, trening, datum

	private Long id;
	private Sala sala; 
	private Trening trening;
	private LocalDateTime vremeOdrzavanja;
	private List<Korisnik> clanovi = new ArrayList<Korisnik>();

	public TerminOdrzavanjaTreninga() {
		super();
	}

	public TerminOdrzavanjaTreninga(Long id, Sala sala, Trening trening, LocalDateTime vremeOdrzavanja,
			List<Korisnik> clanovi) {
		super();
		this.id = id;
		this.sala = sala;
		this.trening = trening;
		this.vremeOdrzavanja = vremeOdrzavanja;
		this.clanovi = clanovi;
	}
	public TerminOdrzavanjaTreninga(Long id, Sala sala, Trening trening, LocalDateTime vremeOdrzavanja) {
		super();
		this.id = id;
		this.sala = sala;
		this.trening = trening;
		this.vremeOdrzavanja = vremeOdrzavanja;
	}
	public TerminOdrzavanjaTreninga(Sala sala, Trening trening, LocalDateTime vremeOdrzavanja,
			List<Korisnik> clanovi) {
		super();
		this.sala = sala;
		this.trening = trening;
		this.vremeOdrzavanja = vremeOdrzavanja;
		this.clanovi = clanovi;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Sala getSala() {
		return sala;
	}

	public void setSala(Sala sala) {
		this.sala = sala;
	}

	public Trening getTrening() {
		return trening;
	}

	public void setTrening(Trening trening) {
		this.trening = trening;
	}

	public LocalDateTime getVremeOdrzavanja() {
		return vremeOdrzavanja;
	}

	public void setVremeOdrzavanja(LocalDateTime vremeOdrzavanja) {
		this.vremeOdrzavanja = vremeOdrzavanja;
	}

	public List<Korisnik> getClanovi() {
		return clanovi;
	}

	public void setClanovi(List<Korisnik> clanovi) {
		this.clanovi = clanovi;
	}

	@Override
	public String toString() {
		return "START "+id + ", " + sala + ", " + trening + ", " + vremeOdrzavanja + ", " + clanovi;
	}
	
}

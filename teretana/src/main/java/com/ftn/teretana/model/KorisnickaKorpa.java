package com.ftn.teretana.model;

import java.util.ArrayList;
import java.util.List;

public class KorisnickaKorpa {

	private Long id;
	private Korisnik korisnik;
	private List<TerminOdrzavanjaTreninga> treninzi = new ArrayList<TerminOdrzavanjaTreninga>();
	
	public KorisnickaKorpa(Korisnik korisnik, List<TerminOdrzavanjaTreninga> trening) {
		super();
		this.korisnik = korisnik;
		this.treninzi = trening;
	}
	public KorisnickaKorpa(Long id, Korisnik korisnik, List<TerminOdrzavanjaTreninga> trening) {
		super();
		this.id = id;
		this.korisnik = korisnik;
		this.treninzi = trening;
	}
	public KorisnickaKorpa(Long id, Korisnik korisnik) {
		super();
		this.id = id;
		this.korisnik = korisnik;
	}
	public KorisnickaKorpa() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public List<TerminOdrzavanjaTreninga> getTreninzi() {
		return treninzi;
	}
	public void setTreninzi(List<TerminOdrzavanjaTreninga> trening) {
		this.treninzi = trening;
	}
	@Override
	public String toString() {
		return "KlijentskaKorpa [korisnik=" + korisnik + ", treninzi=" + treninzi + "]";
	}
	
}

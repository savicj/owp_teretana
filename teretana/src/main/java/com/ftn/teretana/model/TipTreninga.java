package com.ftn.teretana.model;

public class TipTreninga {

	private Long id;
	private String naziv;
	private String opis;
	public TipTreninga(Long id, String naziv, String opis) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.opis = opis;
	}
	public TipTreninga(String naziv, String opis) {
		super();
		this.naziv = naziv;
		this.opis = opis;
	}
	public TipTreninga() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	@Override
	public String toString() {
		return id + ", " + naziv + ", " + opis;
	}
}

package com.ftn.teretana.model;

public class ClanskaKartica {

	private Long id;
	private Korisnik korisnik;
	private int popust;
	private int poeni;
	private boolean odobrena;
	
	public ClanskaKartica(Long id, Korisnik korisnik, int popust, int poeni, boolean odobrena) {
		super();
		this.id = id;
		this.korisnik= korisnik;
		this.popust = popust;
		this.poeni = poeni;
		this.odobrena = odobrena;
	}
	
	public ClanskaKartica(Korisnik korisnik, int popust, int poeni, boolean odobrena) {
		super();
		this.korisnik= korisnik;
		this.popust = popust;
		this.poeni = poeni;
		this.odobrena = odobrena;
	}
	
	public ClanskaKartica() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public int getPopust() {
		return popust;
	}
	public void setPopust(int popust) {
		this.popust = popust;
	}
	public int getPoeni() {
		return poeni;
	}
	public void setPoeni(int poeni) {
		this.poeni = poeni;
	}
	public boolean isOdobrena() {
		return odobrena;
	}

	public void setOdobrena(boolean odobrena) {
		this.odobrena = odobrena;
	}

	@Override
	public String toString() {
		return korisnik + ", " + id + ", " + popust + ", " + poeni + ", " + odobrena;
	}
}

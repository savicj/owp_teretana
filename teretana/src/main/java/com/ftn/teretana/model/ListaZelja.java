package com.ftn.teretana.model;

import java.util.ArrayList;
import java.util.List;

public class ListaZelja {

	@Override
	public String toString() {
		return "ListaZelja [id=" + id + ", korisnik=" + korisnik + ", termin=" + termin + "]";
	}
	private Long id;
	private Korisnik korisnik;
	private List<TerminOdrzavanjaTreninga> termin = new ArrayList<TerminOdrzavanjaTreninga>();
	
	public ListaZelja(Long id, Korisnik korisnik) {
		super();
		this.id = id;
		this.korisnik = korisnik;
	}
	public ListaZelja() {
		super();
	}
	
	public ListaZelja(Korisnik korisnik, List<TerminOdrzavanjaTreninga> termin) {
		super();
		this.korisnik = korisnik;
		this.termin = termin;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public List<TerminOdrzavanjaTreninga> getTermin() {
		return termin;
	}
	public void setTermin(List<TerminOdrzavanjaTreninga> termin) {
		this.termin = termin;
	}

}

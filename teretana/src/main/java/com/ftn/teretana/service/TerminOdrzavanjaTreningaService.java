package com.ftn.teretana.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.Sala;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;
import com.ftn.teretana.model.Trening;

public interface TerminOdrzavanjaTreningaService {

	TerminOdrzavanjaTreninga findOne(Long id);
	List<TerminOdrzavanjaTreninga> findForTrening(Long trening);
	List<TerminOdrzavanjaTreninga> findForDatum(LocalDate datum);
	List<TerminOdrzavanjaTreninga> findAll();
	List<TerminOdrzavanjaTreninga> findForSala(Long id);
	List<TerminOdrzavanjaTreninga> findForKorisnik(Long id);
	 List<TerminOdrzavanjaTreninga> slododniTermini(Trening trening);
	 boolean preklapanjeTermina(TerminOdrzavanjaTreninga termin, Korisnik korisnik);
	 boolean preklapanjeTermina(TerminOdrzavanjaTreninga termin);
	String save(Sala salaid, Trening treningid, LocalDateTime vremePocetka);
}

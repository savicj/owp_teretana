package com.ftn.teretana.service;

import java.util.List;

import com.ftn.teretana.model.ClanskaKartica;
import com.ftn.teretana.model.Korisnik;

public interface ClanskaKarticaService {

	ClanskaKartica findOne(Korisnik korisnik);

	List<ClanskaKartica> findAll();

	ClanskaKartica add(ClanskaKartica clanskaKartica);

	ClanskaKartica findOne(Long id);

	void update(ClanskaKartica kartica);

	
}

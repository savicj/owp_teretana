package com.ftn.teretana.service;

import java.util.List;

import com.ftn.teretana.model.Korisnik;

public interface KorisnikService {

	Korisnik findOne(String email, String sifra);

	Korisnik findOne(Long id);
	
	Korisnik save(Korisnik korisnik);

	List<Korisnik> findAll(String korisnickoIme, String uloga);

	Korisnik update(Korisnik izmenjen);

	void block(Long id);

	List<Korisnik> orderByKorisnickoImeASC();

	List<Korisnik> orderByKorisnickoImeDESC();

	List<Korisnik> orderByUlogaASC();

	List<Korisnik> orderByUlogaDESC();

}

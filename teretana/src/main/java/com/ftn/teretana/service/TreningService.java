package com.ftn.teretana.service;

import java.util.List;

import com.ftn.teretana.model.TipTreninga;
import com.ftn.teretana.model.Trening;
import com.ftn.teretana.model.Trening.NivoTreninga;
import com.ftn.teretana.model.Trening.VrstaTreninga;

public interface TreningService {

	List<Trening> findAll(String naziv, TipTreninga tipTreninga, double mincena, double maxcena, String treneri, VrstaTreninga vrstaTreninga, NivoTreninga nivoTreninga);
	List<Trening> findAll(String naziv, TipTreninga tipTreninga, double mincena, double maxcena, String treneri, VrstaTreninga vrstaTreninga, NivoTreninga nivoTreninga, String admin);
	Trening findOne(Long id);
	void save(Trening trening);
	void delete(Long id);
	Trening update(Trening trening);
	List<Trening> orderByNazivASC();
	List<Trening> orderByNazivDESC();
	List<Trening> orderByTipTreningaASC();
	List<Trening> orderByTipTreningaDESC();
	List<Trening> orderByCenaASC();
	List<Trening> orderByCenaDESC();
	List<Trening> orderByTreneriASC();
	List<Trening> orderByTreneriDESC();
	List<Trening> orderByVrstaTreningaASC();
	List<Trening> orderByVrstaTreningaDESC();
	List<Trening> orderByNivoTreningaASC();
	List<Trening> orderByNivoTreningaDESC();
	List<Trening> orderByOcenaASC();
	List<Trening> orderByOcenaDESC();
}

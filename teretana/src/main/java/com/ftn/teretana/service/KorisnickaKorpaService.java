package com.ftn.teretana.service;

import java.util.List;

import com.ftn.teretana.model.KorisnickaKorpa;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;

public interface KorisnickaKorpaService {
	
	KorisnickaKorpa findForKorisnik(Korisnik korisnik);
	void add(Long id, Long id2);
	void delete(Long korisnik, Long termin);
	String zakazi(TerminOdrzavanjaTreninga termin, Korisnik korisnik);

}

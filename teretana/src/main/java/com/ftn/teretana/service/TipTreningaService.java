package com.ftn.teretana.service;

import java.util.List;

import com.ftn.teretana.model.TipTreninga;

public interface TipTreningaService {
	
	List<TipTreninga> findAll();
	TipTreninga findOne(Long id);
	TipTreninga findByNaziv(String naziv);
	List<TipTreninga> find(Long[] tipTreninga);

}

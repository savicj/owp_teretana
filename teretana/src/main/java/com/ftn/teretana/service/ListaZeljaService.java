package com.ftn.teretana.service;

import java.util.List;

import com.ftn.teretana.model.TerminOdrzavanjaTreninga;

public interface ListaZeljaService {

	List<TerminOdrzavanjaTreninga> findForKorisnik(Long id);

	int delete(Long terminid, Long korisnikid);

	String add(Long id, Long id2);

	
}

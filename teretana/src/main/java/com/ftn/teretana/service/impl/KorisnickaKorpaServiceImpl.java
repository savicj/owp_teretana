package com.ftn.teretana.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.teretana.dao.KorisnickaKorpaDAO;
import com.ftn.teretana.model.KorisnickaKorpa;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;
import com.ftn.teretana.service.KorisnickaKorpaService;
import com.ftn.teretana.service.TerminOdrzavanjaTreningaService;

@Service
public class KorisnickaKorpaServiceImpl implements KorisnickaKorpaService{

	@Autowired
	private KorisnickaKorpaDAO korpaDAO;
	@Autowired
	private TerminOdrzavanjaTreningaService terminService;
	
	@Override
	public KorisnickaKorpa findForKorisnik(Korisnik korisnik) {
		KorisnickaKorpa korpa = korpaDAO.findForKorisnik(korisnik);
		
		return korpa;
	}

	
	@Override
	public void add(Long id, Long id2) {
		korpaDAO.add(id, id2);
		
	}

	@Override
	public void delete(Long korisnik, Long termin) {
		korpaDAO.delete(korisnik, termin);
		
	}

	@Override
	public String zakazi(TerminOdrzavanjaTreninga termin, Korisnik korisnik) {
		String poruka= "";

		List<Korisnik> clanovi = termin.getClanovi();
		int kapacitet = termin.getSala().getKapacitet();
		if(kapacitet <= clanovi.size())
			poruka = "TERMIN JE POPUNJEN";
		if(!clanovi.isEmpty() || clanovi != null)
			for(Korisnik clan : clanovi) {
				if(clan != null)
					if(clan.getId() == korisnik.getId()) {
						 poruka = "KORISNIK JE VEC ZAKAZAO TERMIN!";
					}
			}
		if(terminService.preklapanjeTermina(termin, korisnik))
			poruka = "korisnik ima zakazan termin koji se preklapa sa ovim";
		if(!terminService.preklapanjeTermina(termin, korisnik)) {
			korpaDAO.zakazi(termin, korisnik);
			delete(korisnik.getId(), termin.getId());		
		}
		return poruka;
	}

}

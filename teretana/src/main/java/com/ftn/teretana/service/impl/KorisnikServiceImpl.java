package com.ftn.teretana.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.teretana.dao.KorisnikDAO;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.service.KorisnikService;

@Service
public class KorisnikServiceImpl implements KorisnikService{

	@Autowired
	private KorisnikDAO korisnikDAO;
	
	@Override
	public Korisnik findOne(String email, String sifra) {
		return korisnikDAO.findOne(email, sifra);
	}

	@Override
	public Korisnik save(Korisnik korisnik) {
		korisnikDAO.save(korisnik);
		return korisnik;
		
	}

	@Override
	public List<Korisnik> findAll(String korisnickoIme, String uloga) {
		return korisnikDAO.findAll(korisnickoIme, uloga);
	}

	@Override
	public Korisnik findOne(Long id) {
		return korisnikDAO.findOne(id);

	}

	@Override
	public Korisnik update(Korisnik izmenjen) {
		korisnikDAO.update(izmenjen);
		return izmenjen;
	}
	
	@Override
	public void block(Long id) {
		korisnikDAO.block(id);	
	}
	
	@Override
	public List<Korisnik> orderByKorisnickoImeASC() {
		return korisnikDAO.orderByKorisnickoImeASC();
	}

	@Override
	public List<Korisnik> orderByKorisnickoImeDESC() {
		return korisnikDAO.orderByKorisnickoImeDESC();
	}

	@Override
	public List<Korisnik> orderByUlogaASC() {
		return korisnikDAO.orderByUlogaASC();
	}
	
	@Override
	public List<Korisnik> orderByUlogaDESC() {
		return korisnikDAO.orderByUlogaASC();
	}
}

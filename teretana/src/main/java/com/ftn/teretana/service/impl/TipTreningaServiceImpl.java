package com.ftn.teretana.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.teretana.dao.TipTreningaDAO;
import com.ftn.teretana.model.TipTreninga;
import com.ftn.teretana.service.TipTreningaService;

@Service
public class TipTreningaServiceImpl implements TipTreningaService {

	@Autowired
	private TipTreningaDAO tipTreningaDAO;
	
	@Override
	public TipTreninga findOne(Long id) {
		return tipTreningaDAO.findOne(id);
	}

	@Override
	public TipTreninga findByNaziv(String naziv) {
		return tipTreningaDAO.findByNaziv(naziv);
	}

	@Override
	public List<TipTreninga> findAll() {
		return tipTreningaDAO.findAll();
	}

	@Override
	public List<TipTreninga> find(Long[] tipTreninga) {
		List<TipTreninga> tipovi = new ArrayList<TipTreninga>();
		for(Long id : tipTreninga) {
			tipovi.add(findOne(id));
		}
		return tipovi;
	}

}

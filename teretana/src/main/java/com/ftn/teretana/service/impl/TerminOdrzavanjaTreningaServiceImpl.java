package com.ftn.teretana.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.teretana.dao.TerminOdrzavanjaTreningaDAO;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.Sala;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;
import com.ftn.teretana.model.Trening;
import com.ftn.teretana.service.SalaService;
import com.ftn.teretana.service.TerminOdrzavanjaTreningaService;
import com.ftn.teretana.service.TreningService;

@Service
public class TerminOdrzavanjaTreningaServiceImpl implements TerminOdrzavanjaTreningaService{

	public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-mm-dd hh:mm:ss");

	@Autowired
	private TerminOdrzavanjaTreningaDAO totDAO;

	
	@Override
	public TerminOdrzavanjaTreninga findOne(Long id) {
		return totDAO.findOne(id);
	}

	@Override
	public List<TerminOdrzavanjaTreninga> findForTrening(Long trening) {
		return totDAO.findForTrening(trening);
	}

	@Override
	public List<TerminOdrzavanjaTreninga> findForDatum(LocalDate datum) {
		return totDAO.findForDatum(datum);
	}

	@Override
	public List<TerminOdrzavanjaTreninga> findAll() {
		return totDAO.findAll();
	}


	@Override
	public List<TerminOdrzavanjaTreninga> slododniTermini(Trening trening){
		List<TerminOdrzavanjaTreninga> termini = findForTrening(trening.getId());
		List<TerminOdrzavanjaTreninga> nijePopunjenKapacitet = new ArrayList<TerminOdrzavanjaTreninga>();
		if(termini != null && !termini.isEmpty()) {
			for(TerminOdrzavanjaTreninga termin : termini) {
				if(termin.getSala().getKapacitet() <= termin.getClanovi().size())
					continue;
				else if(termin.getSala().getKapacitet() > termin.getClanovi().size())
					nijePopunjenKapacitet.add(termin);
			}			
		}
		return nijePopunjenKapacitet;
	}

	@Override
	public List<TerminOdrzavanjaTreninga> findForSala(Long id) {
		return totDAO.findForSala(id);
	}

	@Override
	public List<TerminOdrzavanjaTreninga> findForKorisnik(Long id) {
		return totDAO.findForKorisnik(id);
	}

	@Override
	public boolean preklapanjeTermina(TerminOdrzavanjaTreninga termin, Korisnik korisnik) {
		List<TerminOdrzavanjaTreninga> preklopljeniTermini = totDAO.preklapanjeTermina(termin, korisnik);
		boolean preklapaSe= true;
		if(preklopljeniTermini.isEmpty() || preklopljeniTermini == null)
			preklapaSe = false;
		return preklapaSe;
	}

	
	@Override
	public boolean preklapanjeTermina(TerminOdrzavanjaTreninga termin) {
		List<TerminOdrzavanjaTreninga> preklopljeniTermini = totDAO.preklapanjeTermina(termin);
		boolean preklapaSe= true;
		if(preklopljeniTermini.isEmpty() || preklopljeniTermini == null)
			preklapaSe = false;
		return preklapaSe;
	}

	@Override
	public String save(Sala sala, Trening trening, LocalDateTime vremeOdrzavanja) {
		
		TerminOdrzavanjaTreninga termin = new TerminOdrzavanjaTreninga();
		if(sala!=null) 
			termin.setSala(sala);
		if(trening!=null)
			termin.setTrening(trening);
		termin.setVremeOdrzavanja(vremeOdrzavanja);
		termin.setClanovi(new ArrayList<Korisnik>());
			
		boolean preklapanjeTermina = preklapanjeTermina(termin);
		int sacuvan;
		String uspeh = "";
		if(!preklapanjeTermina) {
			totDAO.save(termin);
			uspeh = "sacuvan termin";
		}
		if(preklapanjeTermina)
			uspeh = "preklapa se sa drugim terminom";
		return uspeh;
		
	}


}

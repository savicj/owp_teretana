package com.ftn.teretana.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.teretana.dao.ListaZeljaDAO;
import com.ftn.teretana.dao.TerminOdrzavanjaTreningaDAO;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;
import com.ftn.teretana.service.ListaZeljaService;

@Service
public class ListaZeljaServiceImpl implements ListaZeljaService{

	@Autowired
	private ListaZeljaDAO listaZeljaDAO;
	@Autowired
	private TerminOdrzavanjaTreningaDAO terminDAO;

	@Override
	public List<TerminOdrzavanjaTreninga> findForKorisnik(Long id) {
		return listaZeljaDAO.findForKorisnik(id);
	}

	@Override
	public int delete(Long terminid, Long korisnikid) {
		return listaZeljaDAO.delete(terminid, korisnikid);
	}

	@Override
	public String add(Long id, Long id2) {
		String uspeh = "";
		List<TerminOdrzavanjaTreninga> lista = findForKorisnik(id);
		if(lista != null && !lista.isEmpty()) {
			
			for(TerminOdrzavanjaTreninga termin : lista) {	
				if(termin != null) {
					if(termin != terminDAO.findOne(id2))
						listaZeljaDAO.add(id, id2);
					else
						uspeh = "termin je vec u listi zelja!";
				}else if(termin == null){
					uspeh = "";
				}
			}
		}else if(lista.isEmpty()) {
			uspeh = "";
			lista = new ArrayList<TerminOdrzavanjaTreninga>();
		}
		listaZeljaDAO.add(id, id2);
		return uspeh;
		
		
	}

}

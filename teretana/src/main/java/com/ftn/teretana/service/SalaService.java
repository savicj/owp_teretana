package com.ftn.teretana.service;

import java.util.List;

import com.ftn.teretana.model.Sala;

public interface SalaService {

	List<Sala> findAll(String oznaka);

	List<Sala> orderDESC();

	List<Sala> orderASC();

	int delete(Long id);

	Sala findOne(Long id);

	Sala update(Sala sala);

	void save(Sala sala);

}

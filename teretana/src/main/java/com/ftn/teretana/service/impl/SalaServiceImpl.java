package com.ftn.teretana.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.teretana.dao.SalaDAO;
import com.ftn.teretana.model.Sala;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;
import com.ftn.teretana.service.SalaService;
import com.ftn.teretana.service.TerminOdrzavanjaTreningaService;

@Service
public class SalaServiceImpl implements SalaService{

	@Autowired
	private SalaDAO salaDAO;
	@Autowired
	private TerminOdrzavanjaTreningaService terminService;

	@Override
	public List<Sala> findAll(String oznaka) {
		return salaDAO.findAll(oznaka);
	}

	@Override
	public List<Sala> orderDESC() {
		return salaDAO.orderDESC();

	}

	@Override
	public List<Sala> orderASC() {
		return salaDAO.orderASC();

	}

	@Override
	public int delete(Long id) {
		List<TerminOdrzavanjaTreninga> termin = terminService.findForSala(id);
		if( termin == null) {
			salaDAO.delete(id);	
			return 1;
		}
		else
			return 0;
		
	}

	@Override
	public Sala findOne(Long id) {
		return salaDAO.findOne(id);
	}

	
	@Override
	public void save(Sala sala) {
		salaDAO.save(sala);
		
	}

	@Override
	public Sala update(Sala sala) {
		return salaDAO.update(sala);	

	}

}

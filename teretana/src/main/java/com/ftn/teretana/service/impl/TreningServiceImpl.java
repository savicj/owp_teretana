package com.ftn.teretana.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.teretana.dao.TreningDAO;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;
import com.ftn.teretana.model.TipTreninga;
import com.ftn.teretana.model.Trening;
import com.ftn.teretana.model.Trening.NivoTreninga;
import com.ftn.teretana.model.Trening.VrstaTreninga;
import com.ftn.teretana.service.TerminOdrzavanjaTreningaService;
import com.ftn.teretana.service.TreningService;

@Service
public class TreningServiceImpl implements TreningService{

	@Autowired
	private TreningDAO treningDAO;
	@Autowired
	private TerminOdrzavanjaTreningaService terminiService;
	
	@Override
	public 	List<Trening> findAll(String naziv, TipTreninga tipTreninga, double mincena, double maxcena, String treneri, VrstaTreninga vrstaTreninga, NivoTreninga nivoTreninga) {
		List<Trening> postojiTermin = new ArrayList<Trening>();
		List<TerminOdrzavanjaTreninga> termini = terminiService.findAll();
		for(TerminOdrzavanjaTreninga t : termini) {
			Trening tt = t.getTrening();
			if(!postojiTermin.contains(tt))
				postojiTermin.add(tt);
		}
		List<Trening> treninzi = new ArrayList<Trening>(new HashSet<Trening>(postojiTermin));
			
		return treninzi;// treningDAO.findAll(naziv, tipTreninga, mincena, maxcena, treneri, vrstaTreninga, nivoTreninga);
	}
	
	@Override
	public List<Trening> findAll(String naziv, TipTreninga tipTreninga, double mincena, double maxcena, String treneri, VrstaTreninga vrstaTreninga, NivoTreninga nivoTreninga, String admin) {
		return treningDAO.findAll(naziv, tipTreninga, mincena, maxcena, treneri, vrstaTreninga, nivoTreninga);
	}

	@Override
	public Trening findOne(Long id) {
		return treningDAO.findOne(id);
	}

	@Override
	public void save(Trening trening) {
		treningDAO.save(trening);
	}

	@Override
	public void delete(Long id) {
		treningDAO.delete(id);
	}

	@Override
	public Trening update(Trening trening) {
		treningDAO.update(trening);
		return trening;
	}

	@Override
	public List<Trening> orderByNazivASC() {
		return treningDAO.orderByNazivASC();
	}

	@Override
	public List<Trening> orderByNazivDESC() {
		return treningDAO.orderByNazivDESC();
	}

	@Override
	public List<Trening> orderByTipTreningaASC() {
		return treningDAO.orderByTipTreningaASC();
	}

	@Override
	public List<Trening> orderByTipTreningaDESC() {
		return treningDAO.orderByTipTreningaDESC();
	}

	@Override
	public List<Trening> orderByCenaASC() {
		return treningDAO.orderByCenaASC();
	}

	@Override
	public List<Trening> orderByCenaDESC() {
		return treningDAO.orderByCenaDESC();
	}

	@Override
	public List<Trening> orderByTreneriASC() {
		return treningDAO.orderByTreneriASC();
	}

	@Override
	public List<Trening> orderByTreneriDESC() {
		return treningDAO.orderByTreneriDESC();
	}

	@Override
	public List<Trening> orderByVrstaTreningaASC() {
		return treningDAO.orderByVrstaTreningaASC();
	}

	@Override
	public List<Trening> orderByVrstaTreningaDESC() {
		return treningDAO.orderByVrstaTreningaDESC();
	}

	@Override
	public List<Trening> orderByNivoTreningaASC() {
		return treningDAO.orderByNivoTreningaASC();
	}

	@Override
	public List<Trening> orderByNivoTreningaDESC() {
		return treningDAO.orderByNivoTreningaDESC();
	}

	@Override
	public List<Trening> orderByOcenaASC() {
		return treningDAO.orderByOcenaASC();
	}

	@Override
	public List<Trening> orderByOcenaDESC() {
		return treningDAO.orderByOcenaDESC();
	}



}

package com.ftn.teretana.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.teretana.dao.ClanskaKarticaDAO;
import com.ftn.teretana.model.ClanskaKartica;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.service.ClanskaKarticaService;

@Service
public class ClanskaKarticaServiceImpl implements ClanskaKarticaService{

	@Autowired
	private ClanskaKarticaDAO karticaDAO;
	
	@Override
	public ClanskaKartica findOne(Korisnik korisnik) {
		return karticaDAO.findOne(korisnik);
	}

	@Override
	public List<ClanskaKartica> findAll() {
		return karticaDAO.findAll();
	}

	@Override
	public ClanskaKartica add(ClanskaKartica clanskaKartica) {
		karticaDAO.save(clanskaKartica);
		return clanskaKartica;
	}

	@Override
	public ClanskaKartica findOne(Long id) {
		return karticaDAO.findOne(id);
	}

	@Override
	public void update(ClanskaKartica kartica) {
		karticaDAO.update(kartica);
	}

	
}

package com.ftn.teretana.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.Sala;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;
import com.ftn.teretana.model.Trening;
import com.ftn.teretana.model.Korisnik.Uloga;
import com.ftn.teretana.service.KorisnikService;
import com.ftn.teretana.service.SalaService;
import com.ftn.teretana.service.TerminOdrzavanjaTreningaService;
import com.ftn.teretana.service.TreningService;

@Controller
@RequestMapping(value = "/termini")
public class TerminConroller implements ServletContextAware{

	public static final String KORISNIK_KEY = "korisnik";

	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-mm-dd hh:mm:ss");

	@Autowired
	private TerminOdrzavanjaTreningaService terminService;
	@Autowired
	private SalaService salaService;
	@Autowired
	private TreningService treningService;
	
	
	/** inicijalizacija podataka za kontroler */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 
	
	
	@GetMapping("/details")
	public ModelAndView getOne(@RequestParam Long id, HttpServletResponse response, HttpServletRequest request) throws IOException {
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		TerminOdrzavanjaTreninga termin = terminService.findOne(id);
		if(termin == null) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
			
		
		ModelAndView rezultat = new ModelAndView("termin");
		rezultat.addObject("termin", termin);
		return rezultat;
		
	}
	@GetMapping("/add")
	public ModelAndView add(@RequestParam Long treningid, HttpServletResponse response, HttpServletRequest request) throws IOException{
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null || korisnik.getUloga()!=Uloga.ADMINISTRATOR) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		ModelAndView rezultat = new ModelAndView("dodavanjeTermina"); 
		rezultat.addObject("trening", treningService.findOne(treningid));
		rezultat.addObject("sale", salaService.findAll(""));
		
		return rezultat;
	}

	@PostMapping(value="/add")
	public void add(
			@RequestParam Long salaid,
			@RequestParam Long treningid, 
			@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime vremeOdrzavanja,
			HttpServletResponse response, HttpServletRequest request) throws IOException {			
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null || korisnik.getUloga()!=Uloga.ADMINISTRATOR) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		Sala sala = salaService.findOne(salaid);
		Trening trening = treningService.findOne(treningid);
			
		terminService.save(sala, trening, vremeOdrzavanja);
		
		response.sendRedirect(bURL+"treninzi/details?id="+treningid);
	}
}

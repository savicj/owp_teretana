package com.ftn.teretana.controller;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;

import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;
import com.ftn.teretana.service.KorisnickaKorpaService;
import com.ftn.teretana.service.KorisnikService;
import com.ftn.teretana.service.ListaZeljaService;
import com.ftn.teretana.service.TerminOdrzavanjaTreningaService;

@Controller
@RequestMapping(value = "/listaZelja")
public class ListaZeljaController  implements ServletContextAware{

	public static final String KORISNIK_KEY = "korisnik";

	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private TerminOdrzavanjaTreningaService terminService;
	@Autowired
	private ListaZeljaService listaZeljaService;
	/** inicijalizacija podataka za kontroler */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 
	
	@PostMapping("/add")
	public void dodajUListuZelja(@RequestParam Long terminid, HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik korisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);
		String greska= "";
		if(korisnik == null) {
			greska = "Morate se prvo prijaviti!" ;
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		TerminOdrzavanjaTreninga termin = terminService.findOne(terminid);
		String uspeh = listaZeljaService.add(korisnik.getId(), termin.getId());
		if(uspeh == "")
			response.sendRedirect(bURL+"treninzi/details?id="+termin.getTrening().getId());
		else
			response.sendError(400, uspeh);
	}
	
	@PostMapping(value = "/delete")
	public void delete(@RequestParam Long terminid, @RequestParam Long korisnikid, HttpServletResponse response) throws IOException {
		
		int uspeh = listaZeljaService.delete(terminid, korisnikid);
		if(uspeh == 1)
			response.sendRedirect(bURL+ "korisnici/details?id="+korisnikid);
		else if(uspeh == 0)
			response.sendError(400, "greska!!");
	}
}

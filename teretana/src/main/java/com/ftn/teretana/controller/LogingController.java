package com.ftn.teretana.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.teretana.model.ClanskaKartica;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.Korisnik.Uloga;
import com.ftn.teretana.service.ClanskaKarticaService;
import com.ftn.teretana.service.KorisnikService;


@Controller
@RequestMapping(value = "/")
public class LogingController implements ServletContextAware{

	public static final String KORISNIK_KEY = "korisnik";
	public static final String KARTICA_KEY = "kartica";
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private ClanskaKarticaService karticaService;
	
	/** inicijalizacija podataka za kontroler */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@GetMapping(value = "login")
	public ModelAndView getLogin(HttpSession session, HttpServletResponse response) throws IOException {
		String greska = "";
		if (session.getAttribute(KORISNIK_KEY) != null)
			greska = "korisnik je već prijavljen na sistem morate se prethodno odjaviti<br/>";

		if(!greska.equals("")) {
			response.sendRedirect(bURL);
		}
		return new ModelAndView("login");
	}

	@PostMapping(value = "login")
	@ResponseBody
	public ModelAndView postLogin(@RequestParam(required = false) String korisnickoIme, @RequestParam(required = false) String lozinka,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik korisnik = korisnikService.findOne(korisnickoIme, lozinka);
		ModelAndView mav;
		String greska = "";
		if (korisnik == null) {
			greska = "neispravni kredencijali";
			mav = new ModelAndView("login");
			mav.addObject("greska", greska);
			return mav;
		}

		if (session.getAttribute(KORISNIK_KEY) != null) {
			greska = "korisnik je već prijavljen na sistem morate se prethodno odjaviti<br/>";
			mav = new ModelAndView("treninzi");
			mav.addObject("greska", greska);
			response.sendRedirect(bURL);
			return mav;
		}
		ClanskaKartica kartica = karticaService.findOne(korisnik);
		if(kartica != null && !kartica.isOdobrena())
			session.setAttribute(KARTICA_KEY, kartica);
		session.setAttribute(KORISNIK_KEY, korisnik);
		mav = new ModelAndView("treninzi");
		response.sendRedirect(bURL);
		return null;
	}
	
	
	@GetMapping(value="logout")
	@ResponseBody
	public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {	

		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null) {
			greska="korisnik nije prijavljen<br/>";
			System.err.println(greska);
		}
		
		request.getSession().removeAttribute(KORISNIK_KEY);
		if(request.getSession().getAttribute(KARTICA_KEY) != null)
			request.getSession().removeAttribute(KARTICA_KEY);
		request.getSession().invalidate();
		response.sendRedirect(bURL);
	}
	
	@GetMapping(value = "registracija")
	public ModelAndView getRegistracija(HttpSession session, HttpServletResponse response) throws IOException {
		String greska = "";
		if (session.getAttribute(KORISNIK_KEY) != null)
			greska = "korisnik je već prijavljen na sistem morate se prethodno odjaviti<br/>";

		if(!greska.equals("")) {
			response.sendRedirect(bURL);
		}
		return new ModelAndView("registracija");
	}
	@PostMapping(value = "registracija")
	public void registracija(
			@RequestParam(required = true) String korisnickoIme, 
			@RequestParam(required = true) String lozinka,
			@RequestParam(required = true) String email,
			@RequestParam(required = true) String ime,
			@RequestParam(required = true) String prezime,
			@RequestParam(required = true) String datumRodjenja, 
			@RequestParam(required = true) String adresa,
			@RequestParam(required = true) String brojTelefona,
			HttpSession session, HttpServletResponse response) throws IOException {
		LocalDate datum = LocalDate.parse(datumRodjenja);
		Korisnik korisnik = new Korisnik(korisnickoIme, lozinka, email, ime, prezime, datum, adresa, brojTelefona, LocalDateTime.now(), Uloga.CLAN, false);
		korisnikService.save(korisnik);

		response.sendRedirect(bURL + "login");
	}
	
}

package com.ftn.teretana.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.teretana.model.ClanskaKartica;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.Korisnik.Uloga;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;
import com.ftn.teretana.service.ClanskaKarticaService;
import com.ftn.teretana.service.KorisnikService;
import com.ftn.teretana.service.ListaZeljaService;
import com.ftn.teretana.service.TerminOdrzavanjaTreningaService;


@Controller
@RequestMapping(value = "/korisnici")
public class KorisnikController  implements ServletContextAware{

	public static final String KORISNIK_KEY = "korisnik";
	public static final String KORISNICI_KEY = "korisnici";
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private TerminOdrzavanjaTreningaService terminService;
	@Autowired
	private ListaZeljaService listaZeljaService;
	@Autowired
	private ClanskaKarticaService karticaService;
	
	/** inicijalizacija podataka za kontroler */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@GetMapping
	@ResponseBody
	public ModelAndView getKorisnici(
			@RequestParam(required=false) String korisnickoIme,
			@RequestParam(required=false) String uloga,
			HttpSession session,
			HttpServletRequest request, HttpServletResponse response) throws IOException{
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null || korisnik.getUloga()!=Korisnik.Uloga.ADMINISTRATOR) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		List<Korisnik> korisnici = korisnikService.findAll(korisnickoIme, uloga);

		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("korisnici"); // naziv template-a
		rezultat.addObject(KORISNICI_KEY, korisnici); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	@GetMapping("/details")
	public ModelAndView getOne(@RequestParam Long id, HttpServletResponse response, HttpServletRequest request) throws IOException {
		Korisnik k = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(k==null ) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		Korisnik korisnik = korisnikService.findOne(id);
		if(korisnik==null) {
			System.out.println("Korisnik nije pronadjen");
			response.sendRedirect(bURL);
		}
		List<TerminOdrzavanjaTreninga> listaZelja = listaZeljaService.findForKorisnik(id);
		List<TerminOdrzavanjaTreninga> zakazani = terminService.findForKorisnik(id);
		if(zakazani == null)
			zakazani = new ArrayList<TerminOdrzavanjaTreninga>();
		ClanskaKartica kartica = karticaService.findOne(korisnik);
		ModelAndView rezultat = new ModelAndView("korisnik");
		rezultat.addObject(KORISNIK_KEY, korisnik);
		rezultat.addObject("kartica", kartica);
		rezultat.addObject("treninzi", zakazani);
		rezultat.addObject("listaZelja", listaZelja);

		return rezultat;
		
	}
	@PostMapping(value="/edit")
	public void Edit(@RequestParam Long id, 
			@RequestParam String korisnickoIme,
			@RequestParam String lozinka,
			@RequestParam String email, 
			@RequestParam String ime, 
			@RequestParam String prezime, 
			@RequestParam String datumRodjenja,
			@RequestParam String adresa, 
			@RequestParam String brojTelefona, 
			@RequestParam String datumIVremeRegistracije,
			@RequestParam Uloga uloga,
			HttpServletResponse response, HttpServletRequest request) throws IOException {
		
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null  || korisnik.getUloga()!=Korisnik.Uloga.ADMINISTRATOR) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		Korisnik izmenjen = korisnikService.findOne(id);
		if(korisnik==null) {
			greska="nepostojeci korisnik";
			System.err.println(greska);
		}
		if(izmenjen != null) {
			if(korisnickoIme != null)
				izmenjen.setKorisnickoIme(korisnickoIme);
			if(lozinka != null)
				izmenjen.setLozinka(lozinka);
			if(email != null)
				izmenjen.setEmail(email);
			if(ime != null)
				izmenjen.setIme(ime);
			if(prezime != null)
				izmenjen.setPrezime(prezime);
			if(datumRodjenja != null) {
				LocalDate datum = LocalDate.parse(datumRodjenja);
				izmenjen.setDatumRodjenja(datum);
			}
			if(adresa != null)
				izmenjen.setAdresa(adresa);	
			if(brojTelefona != null)
				izmenjen.setBrojTelefona(brojTelefona);
			if(datumIVremeRegistracije != null) {
				LocalDateTime datum = LocalDateTime.parse(datumIVremeRegistracije);
				izmenjen.setDatumIVremeRegistracije(datum);
			}
			if(uloga != null)
				izmenjen.setUloga(uloga);
			}
		@SuppressWarnings("unused")
		Korisnik sacuvan = korisnikService.update(izmenjen);
		response.sendRedirect(bURL+"korisnici/details?id="+id);

	}
	
	@PostMapping("/blokiraj")
	public void blokiraj(@RequestParam Long id, HttpServletResponse response, HttpServletRequest request) throws IOException {
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null  || korisnik.getUloga()!=Korisnik.Uloga.ADMINISTRATOR) {
			greska="zabranjen pristup";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		Korisnik k = korisnikService.findOne(id);
		if(k==null) {
			greska="nije pronadjen";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		korisnikService.block(id);

		response.sendRedirect(bURL+"korisnici");
	}
	
	
	@GetMapping("/OrderByKorisnickoImeASC")
	public ModelAndView orderByKorisnickoImeASC() {
		
		ModelAndView model = new ModelAndView("korisnici");
		model.addObject(KORISNICI_KEY, korisnikService.orderByKorisnickoImeASC());
		
		return model;
	}
	
	@GetMapping("/OrderByKorisnickoImeDESC")
	public ModelAndView orderByKorisnickoImeDESC() {
		
		ModelAndView model = new ModelAndView("korisnici");
		model.addObject(KORISNICI_KEY, korisnikService.orderByKorisnickoImeDESC());
		
		return model;
	}
		
	@GetMapping("/OrderByUlogaASC")
	public ModelAndView orderByUlogaASC() {
		
		ModelAndView model = new ModelAndView("korisnici");
		model.addObject(KORISNICI_KEY, korisnikService.orderByUlogaASC());
		
		return model;
	}
	@GetMapping("/OrderByUlogaDESC")
	public ModelAndView orderByUlogaDESC() {
		
		ModelAndView model = new ModelAndView("korisnici");
		model.addObject(KORISNICI_KEY, korisnikService.orderByUlogaDESC());
		
		return model;
	}
	
}

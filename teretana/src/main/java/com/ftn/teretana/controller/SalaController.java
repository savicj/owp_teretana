package com.ftn.teretana.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.Sala;
import com.ftn.teretana.model.Korisnik.Uloga;
import com.ftn.teretana.service.SalaService;

@Controller
@RequestMapping("/sale")
public class SalaController implements ServletContextAware{

	public static final String KORISNIK_KEY = "korisnik";
	public static final String SALE_KEY = "sale";

	@Autowired
	private ServletContext servletContext;
	private  String bURL;

	@Autowired
	private SalaService salaService; 
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;		
	}

	@GetMapping
	public ModelAndView get(
			@RequestParam(required=false) String oznaka, 
			HttpSession session, HttpServletResponse response, HttpServletRequest request) throws IOException{
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null || korisnik.getUloga()!=Korisnik.Uloga.ADMINISTRATOR) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		List<Sala> sale = salaService.findAll(oznaka);

		ModelAndView rezultat = new ModelAndView("sale");
		rezultat.addObject(SALE_KEY, sale);

		return rezultat;
	}
	@GetMapping("/OrderByOznakaASC")
	public ModelAndView orderByNazivASC() {
			
		ModelAndView model = new ModelAndView("sale");
		model.addObject(SALE_KEY, salaService.orderASC());
		
		return model;
	}
	@GetMapping("/OrderByOznakaDESC")
	public ModelAndView orderByNazivDESC() {
		ModelAndView model = new ModelAndView("sale");
		model.addObject(SALE_KEY, salaService.orderDESC());
		
		return model;
	}
	@GetMapping("/details")
	public ModelAndView getOne(@RequestParam Long id, HttpServletResponse response) throws IOException {
		Sala sala = salaService.findOne(id);
		if(sala==null) {
			System.out.println("sala nije pronadjena");
			response.sendRedirect(bURL+"sale");
		}
		
		ModelAndView rezultat = new ModelAndView("sala");
		rezultat.addObject("sala", sala);
		return rezultat;
		
	}
	@GetMapping("/add")
	public ModelAndView add(HttpServletResponse response, HttpServletRequest request) throws IOException{
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null || korisnik.getUloga()!=Uloga.ADMINISTRATOR) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		ModelAndView rezultat = new ModelAndView("dodajSalu"); 		
		return rezultat;
	}

	@PostMapping(value="/add")
	public void add(@RequestParam String oznaka,
			@RequestParam int kapacitet, 
			HttpServletResponse response) throws IOException {			
		
		
		Sala sala = new Sala(oznaka, kapacitet);
		salaService.save(sala);
		System.err.println(sala.toString());
		
		response.sendRedirect(bURL+"sale");
	}
	@PostMapping(value="/edit")
	public void Edit(@RequestParam Long id, 
			@RequestParam String oznaka,			 
			@RequestParam int kapacitet, 
			HttpServletResponse response, HttpServletRequest request) throws IOException {
		
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null || korisnik.getUloga()!=Uloga.ADMINISTRATOR) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
		}
		
		Sala sala = salaService.findOne(id);
		if(sala != null) {
			if(oznaka != null && !oznaka.trim().equals(""))
				sala.setOznaka(oznaka);
			if(kapacitet > 0)
				sala.setKapacitet(kapacitet);
			
		}

		Sala saved = salaService.update(sala);
		if(saved!=null)
			response.sendRedirect(bURL+"sale");
		else
			System.err.println(greska);
	}
	@PostMapping(value = "/delete")
	public void delete(@RequestParam Long id, HttpServletResponse response) throws IOException {
		int uspeh = salaService.delete(id);
		if(uspeh == 1)
			response.sendRedirect(bURL+ "sale");
		else if(uspeh == 0)
			response.sendError(400, "postoje zakazani treninzi");
	}
}

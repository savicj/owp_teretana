package com.ftn.teretana.controller;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.teretana.model.ClanskaKartica;
import com.ftn.teretana.model.KorisnickaKorpa;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;
import com.ftn.teretana.model.Trening;
import com.ftn.teretana.service.ClanskaKarticaService;
import com.ftn.teretana.service.KorisnickaKorpaService;
import com.ftn.teretana.service.ListaZeljaService;
import com.ftn.teretana.service.TerminOdrzavanjaTreningaService;

@Controller
@RequestMapping("/korpa")
public class KorpaController implements ServletContextAware{

	public static final String KORISNIK_KEY = "korisnik";
	public static final String TIPOVI_TRENINGA_KEY = "tipovi_treninga";
	public static final String TRENINZI_KEY = "treninzi";
	public static final String TRENING_KEY = "trening";
	public static final String KORPA_KEY = "korpe";
	public static final String TERMINI_ODRZAVANJA_KEY = "termini";
	public static final String KARTICA_KEY = "kartica";

	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	
	@Autowired
	private KorisnickaKorpaService korpaService;
	@Autowired
	private TerminOdrzavanjaTreningaService terminService;
	@Autowired
	private ListaZeljaService listaZeljaService;
	@Autowired
	private ClanskaKarticaService karticaService;
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 
	
	@GetMapping
	public ModelAndView getKorpa(HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik korisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);
		if(korisnik == null) {
			response.sendRedirect(bURL);
			return null;
		}
		KorisnickaKorpa korpe = korpaService.findForKorisnik(korisnik);
		ClanskaKartica kartica = karticaService.findOne(korisnik);
		double suma = 0;
		for(TerminOdrzavanjaTreninga t : korpe.getTreninzi()) {
			suma = suma + t.getTrening().getCena();
		}
//		System.err.println(korpe);
		ModelAndView rez = new ModelAndView("korpa");
		rez.addObject(KORPA_KEY, korpe);
		rez.addObject(KARTICA_KEY, kartica);
		rez.addObject("suma", suma);
		
		return rez;
	
	}
	
	@PostMapping("/add")
	public void dodajUKorpu(@RequestParam Long terminid, HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik korisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);
		String greska= "";
		if(korisnik == null) {
			greska = "Morate se prvo prijaviti!" ;
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		TerminOdrzavanjaTreninga termin = terminService.findOne(terminid);
		korpaService.add(korisnik.getId(), termin.getId());
		listaZeljaService.delete(terminid, korisnik.getId());
		response.sendRedirect(bURL+"treninzi/details?id="+termin.getTrening().getId());
	}
	
	@PostMapping("/obrisi")
	public void odustani(@RequestParam Long terminid, HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik korisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);
		String greska= "";
		if(korisnik == null) {
			greska = "Morate se prvo prijaviti!" ;
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		TerminOdrzavanjaTreninga termin = terminService.findOne(terminid);
		korpaService.delete(korisnik.getId(),termin.getId());
		response.sendRedirect(bURL+"korpa");
	}
	@PostMapping("/zakazi")
	public void zakazisve(HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik korisnik = (Korisnik) session.getAttribute(KORISNIK_KEY);
		String greska= "";
		if(korisnik == null) {
			greska = "Morate se prvo prijaviti!" ;
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		KorisnickaKorpa korpa = korpaService.findForKorisnik(korisnik);
		ClanskaKartica kartica = karticaService.findOne(korisnik);
		double suma = 0;
		for(TerminOdrzavanjaTreninga t : korpa.getTreninzi()) {
			suma = suma + t.getTrening().getCena();
		}
		
		String poruka = "";
		if(korpa != null) {
			for(TerminOdrzavanjaTreninga termin : korpa.getTreninzi()) {
				poruka += korpaService.zakazi(termin, korisnik);
				if(poruka=="") {
					termin.getClanovi().add(korisnik);
					kartica.setPoeni(kartica.getPoeni() + (int) (suma/500));
					System.out.println(suma + "\n"+kartica.getPoeni());
				}
				System.err.println(poruka);
			}
			if(poruka == "")
				response.sendRedirect(bURL + "korpa");
			else
				response.sendError(500, poruka);
		}
	}
}

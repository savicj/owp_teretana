package com.ftn.teretana.controller;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.teretana.model.ClanskaKartica;
import com.ftn.teretana.model.KorisnickaKorpa;
import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.service.ClanskaKarticaService;
import com.ftn.teretana.service.KorisnickaKorpaService;
import com.ftn.teretana.service.KorisnikService;

@Controller
@RequestMapping("/clanskeKartice")
public class ClanskeKarticeController implements ServletContextAware{

	public static final String KORISNIK_KEY = "korisnik";
	public static final String KARTICE_KEY = "kartice";

	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-mm-dd hh:mm:ss");

	@Autowired
	private ClanskaKarticaService karticaService;
	@Autowired
	private KorisnickaKorpaService korpaService;
	
	
	/** inicijalizacija podataka za kontroler */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 
	
	@GetMapping
	@ResponseBody
	public ModelAndView getKartice(	HttpSession session,HttpServletRequest request, HttpServletResponse response) throws IOException{
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null || korisnik.getUloga()!=Korisnik.Uloga.ADMINISTRATOR) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		List<ClanskaKartica> kartice = karticaService.findAll();

		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("clanskeKartice"); // naziv template-a
		rezultat.addObject(KARTICE_KEY, kartice); // podatak koji se šalje template-u
		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}

	@PostMapping
	@RequestMapping("/zahtev")
	public void zahtev(@RequestParam Long id, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Korisnik korisnik = korisnikService.findOne(id);
		ClanskaKartica clanskaKartica;
		if(korisnik != null)
			clanskaKartica = karticaService.add(new ClanskaKartica(korisnik, 10, 10*5, false));
		response.sendRedirect(bURL+"korisnici");
	}
	
	@PostMapping
	@RequestMapping("/odobri")
	public void odobri(@RequestParam Long id, HttpServletRequest request, HttpServletResponse response) throws IOException {
		ClanskaKartica kartica = karticaService.findOne(id);
		if(kartica != null) {
			kartica.setOdobrena(true);
			karticaService.update(kartica);
		}
		response.sendRedirect(bURL+"clanskeKartice");
		
	}
	
	@PostMapping
	@RequestMapping("/iskoristi")
	public ModelAndView iskoristi(@RequestParam Long id, 
							@RequestParam int poeni,
							@RequestParam double suma,
							HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException {
		
		ClanskaKartica kartica = karticaService.findOne(id);
		if(kartica.getPoeni() < poeni)
			response.sendError(400, "Korisnik nema dovoljno poena");
		kartica.setPoeni(kartica.getPoeni() - poeni);
		kartica.setPopust(kartica.getPoeni()*5);
		karticaService.update(kartica);
		suma = suma - (suma*kartica.getPopust()/100);
		KorisnickaKorpa korpe = korpaService.findForKorisnik(kartica.getKorisnik());
		ModelAndView rez = new ModelAndView("korpa");
		rez.addObject("korpe", korpe);
		rez.addObject("kartica", kartica);
		rez.addObject("suma", suma);
		
		return rez;	}
}

package com.ftn.teretana.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.teretana.model.Korisnik;
import com.ftn.teretana.model.Korisnik.Uloga;
import com.ftn.teretana.model.TerminOdrzavanjaTreninga;
import com.ftn.teretana.model.TipTreninga;
import com.ftn.teretana.model.Trening;
import com.ftn.teretana.model.Trening.NivoTreninga;
import com.ftn.teretana.model.Trening.VrstaTreninga;
import com.ftn.teretana.service.TerminOdrzavanjaTreningaService;
import com.ftn.teretana.service.TipTreningaService;
import com.ftn.teretana.service.TreningService;

@Controller
@RequestMapping("/")
public class TreningController implements ServletContextAware{
	
	public static final String KORISNIK_KEY = "korisnik";
	public static final String ULOGOVANI_KORISNIK_KEY = "ulogovani_korisnik";
	public static final String TIPOVI_TRENINGA_KEY = "tipovi_treninga";
	public static final String TRENINZI_KEY = "treninzi";
	public static final String TRENINZI_ADMIN_KEY = "treninziSvi";
	public static final String TRENING_KEY = "trening";
	public static final String TERMINI_ODRZAVANJA_KEY = "termini";

	private static String folder = "C:\\Users\\Jana\\git\\owp_teretana\\teretana\\src\\main\\resources\\static\\slike";


	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private TreningService treninziService;
	@Autowired
	private TipTreningaService tipTreningaService;
	@Autowired
	private TerminOdrzavanjaTreningaService terminiService;
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 
	
	@GetMapping
	public ModelAndView getTreninzi(
			@RequestParam(required=false) String naziv, 
			@RequestParam(required=false) String tipTreninga, 
			@RequestParam(required=false) Double mincena, 
			@RequestParam(required=false) Double maxcena, 
			@RequestParam(required=false) String treneri,
			@RequestParam(required=false) VrstaTreninga vrstaTreninga,
			@RequestParam(required=false) NivoTreninga nivoTreninga,
			HttpSession session, HttpServletResponse response){
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		TipTreninga tip;
		if(tipTreninga != null) {
			tip = tipTreningaService.findByNaziv(tipTreninga);
		}else {
			tip=null;
		}
		if(mincena == null) {
			mincena=(double) 0;
		}
		if(maxcena == null) {
			maxcena=(double) 0;
		}
		
		List<Trening> treninzi = treninziService.findAll(naziv, tip, mincena, maxcena, treneri, vrstaTreninga, nivoTreninga);
		List<Trening> adminTreninzi = treninziService.findAll(naziv, tip, mincena, maxcena, treneri, vrstaTreninga, nivoTreninga, "admin");
		List<TerminOdrzavanjaTreninga> termini = terminiService.findAll();

		ModelAndView rezultat = new ModelAndView("treninzi");
		rezultat.addObject(TRENINZI_KEY, treninzi); 
		rezultat.addObject(TRENINZI_ADMIN_KEY, adminTreninzi); 
		rezultat.addObject("termini", termini);
		rezultat.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);

		return rezultat;
	}
	
	@GetMapping("treninzi/details")
	public ModelAndView getOne(@RequestParam Long id){
		Trening trening = treninziService.findOne(id);
		List<TipTreninga> tipoviZaTrening = trening.getTipTreninga();
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<TerminOdrzavanjaTreninga> termini = terminiService.findForTrening(trening.getId());
		List<TerminOdrzavanjaTreninga> nijePopunjenKapacitet = terminiService.slododniTermini(trening);//new ArrayList<TerminOdrzavanjaTreninga>();
		
		ModelAndView rezultat = new ModelAndView("trening"); 
		rezultat.addObject(TRENING_KEY, trening); 
		rezultat.addObject(TIPOVI_TRENINGA_KEY, tipovi); 
		rezultat.addObject("tipovi_za_trening", tipoviZaTrening); 
		rezultat.addObject(TERMINI_ODRZAVANJA_KEY, termini); 
		rezultat.addObject("nepopunjeni", nijePopunjenKapacitet);

		return rezultat;
	}

	@SuppressWarnings("unused")
	@PostMapping(value="treninzi/edit")
	public void Edit(@RequestParam Long id, 
			@RequestParam String naziv,
			@RequestParam String treneri,
			@RequestParam Long[] tipTreninga, 
			@RequestParam double cena, 
			@RequestParam VrstaTreninga vrstaTreninga,
			@RequestParam NivoTreninga nivoTreninga, 
			@RequestParam int trajanjeTreninga, 
			@RequestParam double prosecnaOcena,
			@RequestParam String opis,
			HttpServletResponse response, HttpServletRequest request) throws IOException {
		
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null || korisnik.getUloga()!=Uloga.ADMINISTRATOR) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
		}
		
		Trening trening = treninziService.findOne(id);
		if(trening != null) {
			if(naziv != null && !naziv.trim().equals(""))
				trening.setNaziv(naziv);
			if(treneri != null && !treneri.trim().equals(""))
				trening.setTreneri(treneri);
			if(tipTreninga != null) {
				trening.setTipTreninga(tipTreningaService.find(tipTreninga));
			}
			if(cena >= 0)
				trening.setCena((double) cena);
			if(vrstaTreninga != null)
				trening.setVrstaTreninga(vrstaTreninga);
			if(nivoTreninga != null)
				trening.setNivoTreninga(nivoTreninga);
			if(trajanjeTreninga >= 0)
				trening.setTrajanjeTreninga(trajanjeTreninga);
			if(prosecnaOcena >= 0 && prosecnaOcena <= 5)
				trening.setProsecnaOcena((double) prosecnaOcena);
			if(opis != null)
				trening.setOpis(opis);
		}

		Trening saved = treninziService.update(trening);
		response.sendRedirect(bURL);
	}
	
	@GetMapping("treninzi/add")
	public ModelAndView add(HttpServletResponse response, HttpServletRequest request) throws IOException{
		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null || korisnik.getUloga()!=Uloga.ADMINISTRATOR) {
			greska="zabranjen pristup<br/>";
			System.err.println(greska);
			response.sendRedirect(bURL);
		}
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		ModelAndView rezultat = new ModelAndView("dodavanjeTreninga"); 
		rezultat.addObject(TIPOVI_TRENINGA_KEY, tipovi); 
		
		return rezultat;
	}

	@PostMapping(value="treninzi/add")
	public void add(@RequestParam String naziv,
			@RequestParam String treneri,
			@RequestParam Long[] tipTreninga, 
			@RequestParam double cena, 
			@RequestParam VrstaTreninga vrstaTreninga,
			@RequestParam NivoTreninga nivoTreninga, 
			@RequestParam int trajanjeTreninga, 
			@RequestParam String opis,
			@RequestParam MultipartFile slika,
			HttpServletResponse response) throws IOException {			
		
		
		byte[] bytes = slika.getBytes();
		Path path = Paths.get(folder + "\\" + slika.getOriginalFilename());
		Files.write(path, bytes);
		Trening trening = new Trening( naziv,  treneri,  opis,  slika.getOriginalFilename(), tipTreningaService.find(tipTreninga), cena,  vrstaTreninga,  nivoTreninga,  trajanjeTreninga, 0);
		
		treninziService.save(trening);
		System.err.println(trening.toString() + "\n" + path.toString());
		
		response.sendRedirect(bURL);
	}
	
	@GetMapping("treninzi/OrderByNazivASC")
	public ModelAndView orderByNazivASC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByNazivASC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByNazivDESC")
	public ModelAndView orderByNazivDESC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByNazivDESC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByTipTreningaASC")
	public ModelAndView orderByTipTreningaASC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByTipTreningaASC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByTipTreningaDESC")
	public ModelAndView orderByTipTreningaDESC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByTipTreningaDESC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByCenaASC")
	public ModelAndView orderByCenaASC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByCenaASC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByCenaDESC")
	public ModelAndView orderByCenaDESC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByCenaDESC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByTreneriASC")
	public ModelAndView orderByTreneriASC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByTreneriASC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByTreneriDESC")
	public ModelAndView orderByTreneriDESC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByTreneriDESC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByVrstaTreningaASC")
	public ModelAndView orderByVrstaTreningaASC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByVrstaTreningaASC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByVrstaTreningaDESC")
	public ModelAndView orderByVrstaTreningaDESC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByVrstaTreningaDESC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByNivoTreningaASC")
	public ModelAndView orderByNivoTreningaASC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByNivoTreningaASC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByNivoTreningaDESC")
	public ModelAndView orderByNivoTreningaDESC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByNivoTreningaDESC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByOcenaASC")
	public ModelAndView orderByOcenaASC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByOcenaASC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
	@GetMapping("treninzi/OrderByOcenaDESC")
	public ModelAndView orderByOcenaDESC() {
		List<TipTreninga> tipovi = tipTreningaService.findAll();
		List<String> tipoviNaziv = new ArrayList<String>();
		for(TipTreninga t : tipovi) {
			tipoviNaziv.add(t.getNaziv());
		}
		
		ModelAndView model = new ModelAndView("treninzi");
		model.addObject(TRENINZI_KEY, treninziService.orderByOcenaDESC());
		model.addObject(TIPOVI_TRENINGA_KEY, tipoviNaziv);
		
		return model;
	}
}

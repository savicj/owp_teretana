DROP DATABASE teretana;
CREATE DATABASE teretana;
USE teretana;

CREATE TABLE korisnici(
	id int PRIMARY KEY AUTO_INCREMENT,
	korisnicko_ime varchar(30),
	lozinka varchar(30),
	email varchar(50),
	ime varchar(30),
	prezime varchar(30),
	datum_rodjenja DATE,
	adresa varchar(50),
	telefon varchar(20),
	datum_vreme_registracije DATETIME,
	uloga varchar(20),
    blokiran boolean
);

INSERT INTO korisnici(korisnicko_ime, lozinka, email, ime, prezime, datum_rodjenja, adresa, telefon, datum_vreme_registracije, uloga, blokiran)
	VALUES('admin', 'admin', 'admin@mail.com', 'Marija', 'Maric', '1996-06-21', 'Karadjordjeva 3','062899843', '2021-11-02 15:30:30', 'ADMINISTRATOR', false);
INSERT INTO korisnici(korisnicko_ime, lozinka, email, ime, prezime, datum_rodjenja, adresa, telefon, datum_vreme_registracije, uloga, blokiran)
	VALUES('clan', 'clan', 'clan@mail.com','Jovan', 'Jovanovic', '1993-05-21', 'Zmaj Jovina 3','062529843', '2021-12-12 14:41:30', 'CLAN', false);
INSERT INTO korisnici(korisnicko_ime, lozinka, email, ime, prezime, datum_rodjenja, adresa, telefon, datum_vreme_registracije, uloga, blokiran)
	VALUES('a', 'a', 'a@mail.com', 'Nevena', 'Maljugic', '1997-10-10', 'Vojvode Putnika 123','063598445', '2021-12-28 08:41:30', 'ADMINISTRATOR', false);
INSERT INTO korisnici(korisnicko_ime, lozinka, email, ime, prezime, datum_rodjenja, adresa, telefon, datum_vreme_registracije, uloga, blokiran)
	VALUES('b', 'b', 'b@mail.com', 'Marko', 'Markovic', '1993-08-18', 'Somborska','062529843', '2021-12-12 14:41:30', 'CLAN', false);
INSERT INTO korisnici(korisnicko_ime, lozinka, email, ime, prezime, datum_rodjenja, adresa, telefon, datum_vreme_registracije, uloga, blokiran)
	VALUES('c', 'c', 'c@mail.com', 'Vasilisa', 'Zivanovic', '1998-09-24', 'Luja Adamica 26','0612904025', '2022-01-05 14:41:30', 'CLAN', true);

	
CREATE TABLE tip_treninga(
	id int PRIMARY KEY AUTO_INCREMENT,
	naziv varchar(20),
	opis text
);

INSERT INTO tip_treninga(naziv, opis) VALUES ('JOGA', 'Joga podrazumeva upotrebu tehnika: fizičkih položaja, vežbi upravljanja energijom putem disanja, relaksaciono-meditativnih vežbi, koncentracije, meditacije i samorealizacije');
INSERT INTO tip_treninga(naziv, opis) VALUES ('TONING', 'Trening za toniranje misica');
INSERT INTO tip_treninga(naziv, opis) VALUES ('CARDIO', 'Full-body trening za sagorevanje kalorija');
INSERT INTO tip_treninga(naziv, opis) VALUES ('STRENGTH', 'Trening jacanja misica');


CREATE TABLE treninzi(
	id int PRIMARY KEY AUTO_INCREMENT,
	naziv varchar(30),
	treneri varchar(50),
	opis varchar(50),
	slika varchar(50),
	cena double,
	vrsta_treninga varchar(30),
	nivo_treninga varchar(30),
	trajanje_treninga int, 
	prosecna_ocena double
);

INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('JOGA', 'Milica, Nenad', 'Relaksaciono-meditacioni trening', 'joga.jpg', 250.00, 'GRUPNI', 'AMATERSKI', 90, 55);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('JOGA', 'Milica, Nenad', 'Relaksaciono-meditacioni trening', 'joga.jpg', 350.00, 'GRUPNI', 'SREDNJI', 90, 5);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('JOGA', 'Milica, Nenad', 'Relaksaciono-meditacioni trening', 'yoga.jpg', 400.00, 'GRUPNI', 'NAPREDNI', 90, 5);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('TONING', 'Nevena', 'Trening toniranja misica', 'toning.jpg', 250, 'GRUPNI', 'AMATERSKI', 60, 5);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('TONING', 'Nevena', 'Trening toniranja misica', 'toning.jpg', 300, 'GRUPNI', 'SREDNJI', 60, 5);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('TONING', 'Mario', 'Trening toniranja misica', 'toning.jpg', 400, 'GRUPNI', 'NAPREDNI', 60, 5);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('CARDIO', 'Nikola', 'Kardio trening za sagorevanje kalorija', 'cardio.jpg', 250, 'GRUPNI', 'AMATERSKI', 45, 5);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('CARDIO', 'Mario', 'Kardio trening za sagorevanje kalorija', 'cardio.jpg', 300, 'GRUPNI', 'SREDNJI', 45, 5);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('CARDIO', 'Mario, Nikola', 'Kardio trening za sagorevanje kalorija', 'cardio.jpg', 250, 'GRUPNI', 'NAPREDNI', 60, 5);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('STRENGTH', 'Steva', 'Jacanje misica', 'strenght.jpeg', 300, 'GRUPNI', 'AMATERSKI', 60, 5);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('STRENGTH', 'Steva', 'Jacanje misica', 'strenght.jpeg', 350, 'GRUPNI', 'SREDNJI', 60, 5);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('STRENGTH', 'Steva, Nikola', 'Jacanje misica', 'strenght.jpeg', 400, 'GRUPNI', 'NAPREDNI', 60, 5);
-- mesani treninzi
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('STRENGTH & TONE', 'Steva, Nikola', 'Jacanje misica', 'strenght.jpeg', 400, 'GRUPNI', 'NAPREDNI', 60, 5);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('STRENGTH & CARDIO', 'Steva, Nikola', 'Jacanje misica', 'strenght.jpeg', 400, 'GRUPNI', 'NAPREDNI', 60, 5);
-- POJEDINACNI
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('CARDIO & TONING', 'Steva, Nikola', 'Jacanje misica', 'personal.jpg', 1000, 'POJEDINACNI', 'NAPREDNI', 90, 5);
INSERT INTO	treninzi(naziv, treneri, opis, slika, cena, vrsta_treninga, nivo_treninga, trajanje_treninga, prosecna_ocena) 
	VALUES('TONING', 'Nevena', 'Trening toniranja misica', 'personal.jpg', 900, 'POJEDINACNI', 'SREDNJI', 60, 5);

	
CREATE TABLE trening_tip(
	trening_id int,
	tip_treninga_id int,
	PRIMARY KEY(trening_id, tip_treninga_id),
	FOREIGN KEY(trening_id) REFERENCES treninzi(id) ON DELETE CASCADE,
	FOREIGN KEY(tip_treninga_id) REFERENCES tip_treninga(id) ON DELETE CASCADE
);

INSERT INTO trening_tip VALUES(1, 1);
INSERT INTO trening_tip VALUES(2, 1);
INSERT INTO trening_tip VALUES(3, 1);
INSERT INTO trening_tip VALUES(4, 2);
INSERT INTO trening_tip VALUES(5, 2);
INSERT INTO trening_tip VALUES(6, 2);
INSERT INTO trening_tip VALUES(7, 3);
INSERT INTO trening_tip VALUES(8, 3);
INSERT INTO trening_tip VALUES(9, 3);
INSERT INTO trening_tip VALUES(10, 4);
INSERT INTO trening_tip VALUES(11, 4);
INSERT INTO trening_tip VALUES(12, 4);

INSERT INTO trening_tip VALUES(13, 4);
INSERT INTO trening_tip VALUES(13, 2);
INSERT INTO trening_tip VALUES(14, 4);
INSERT INTO trening_tip VALUES(14, 3);
INSERT INTO trening_tip VALUES(15, 2);
INSERT INTO trening_tip VALUES(15, 3);
INSERT INTO trening_tip VALUES(16, 2);


CREATE TABLE sale(
	id int PRIMARY KEY AUTO_INCREMENT,
	oznaka varchar(20),
	kapacitet int
);

INSERT INTO sale(oznaka, kapacitet) VALUES('SA1-1', 20);
INSERT INTO sale(oznaka, kapacitet) VALUES('SA1-2', 15);
INSERT INTO sale(oznaka, kapacitet) VALUES('SA2-1', 13);
INSERT INTO sale(oznaka, kapacitet) VALUES('SA2-2', 20);
INSERT INTO sale(oznaka, kapacitet) VALUES('SA3-1', 10);


CREATE TABLE termini_odrzavanja_treninga(
	id int PRIMARY KEY AUTO_INCREMENT,
	sala_id int, 
	trening_id int, 
	vreme DATETIME,
	FOREIGN KEY(sala_id) REFERENCES sale(id) ON DELETE CASCADE,
	FOREIGN KEY(trening_id) REFERENCES treninzi(id) ON DELETE CASCADE
);
-- 08.01.2022.
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(1, 1, '2022-08-01 18:00:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(1, 2, '2022-08-01 19:40:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(1, 3, '2022-08-01 21:15:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(2, 4, '2022-08-01 18:00:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(2, 5, '2022-08-01 19:10:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(2, 6, '2022-08-01 20:20:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(3, 7, '2022-08-01 18:00:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(3, 8, '2022-08-01 19:00:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(3, 9, '2022-08-01 20:00:00');
-- INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
--	VALUES(4, 10, '2022-08-01 18:00:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(4, 11, '2022-08-01 19:10:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(4, 12, '2022-08-01 20:20:00');
	
-- 09.01.2022.
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(1, 1, '2022-09-01 18:00:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(1, 2, '2022-09-01 19:40:00');
-- INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
--	VALUES(1, 3, '2022-09-01 21:15:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(2, 4, '2022-09-01 18:00:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(2, 5, '2022-09-01 19:10:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(2, 6, '2022-09-01 20:20:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(3, 7, '2022-09-01 18:00:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(3, 8, '2022-09-01 19:00:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(3, 9, '2022-09-01 20:00:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(4, 10, '2022-09-01 18:00:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(4, 11, '2022-09-01 19:10:00');
INSERT INTO termini_odrzavanja_treninga(sala_id, trening_id, vreme)
	VALUES(4, 12, '2022-09-01 20:20:00');
	
CREATE TABLE zakazani_termini(
    terminid int,
	korisnikid int,
    FOREIGN KEY(korisnikid) REFERENCES korisnici(id),
	FOREIGN KEY(terminid) REFERENCES termini_odrzavanja_treninga(id)
);    

INSERT INTO zakazani_termini(terminid, korisnikid) VALUES(3,2);
INSERT INTO zakazani_termini(terminid, korisnikid) VALUES(4,2);
INSERT INTO zakazani_termini(terminid, korisnikid) VALUES(6,4);
INSERT INTO zakazani_termini(terminid, korisnikid) VALUES(9,4);
INSERT INTO zakazani_termini(terminid, korisnikid) VALUES(10,5);

CREATE TABLE korisnicka_korpa(
	id int PRIMARY KEY AUTO_INCREMENT,
	korisnikid int,
	FOREIGN KEY(korisnikid) REFERENCES korisnici(id)
);

INSERT INTO korisnicka_korpa(korisnikid) VALUES(2);
INSERT INTO korisnicka_korpa(korisnikid) VALUES(4);

CREATE TABLE korpa_termin(
	korisnikid int,
    terminid int,
	FOREIGN KEY(korisnikid) REFERENCES korisnici(id),
	FOREIGN KEY(terminid) REFERENCES termini_odrzavanja_treninga(id)
);

INSERT INTO korpa_termin(korisnikid, terminid) VALUES(2, 2);
INSERT INTO korpa_termin(korisnikid, terminid) VALUES(2, 5);
INSERT INTO korpa_termin(korisnikid, terminid) VALUES(2, 6);
INSERT INTO korpa_termin(korisnikid, terminid) VALUES(4, 1);
INSERT INTO korpa_termin(korisnikid, terminid) VALUES(4, 6);
INSERT INTO korpa_termin(korisnikid, terminid) VALUES(4, 8);
INSERT INTO korpa_termin(korisnikid, terminid) VALUES(2, 4);
INSERT INTO korpa_termin(korisnikid, terminid) VALUES(4, 2);
INSERT INTO korpa_termin(korisnikid, terminid) VALUES(2, 9);

CREATE TABLE lista_zelja(
	id int PRIMARY KEY auto_increment,
    korisnik_id int,
    FOREIGN KEY(korisnik_id) REFERENCES korisnici(id) ON DELETE CASCADE
);

INSERT INTO lista_zelja(korisnik_id) VALUES(2);
INSERT INTO lista_zelja(korisnik_id) VALUES(4);

CREATE TABLE zelje(
    korisnik_id int,
    termin_id int,
    FOREIGN KEY(korisnik_id) REFERENCES korisnici(id) ON DELETE CASCADE,
    FOREIGN KEY(termin_id) REFERENCES termini_odrzavanja_treninga(id) ON DELETE CASCADE
);

INSERT INTO zelje(korisnik_id, termin_id) VALUES(2, 1);
INSERT INTO zelje(korisnik_id, termin_id) VALUES(2, 5);
INSERT INTO zelje(korisnik_id, termin_id) VALUES(2, 7);

INSERT INTO zelje(korisnik_id, termin_id) VALUES(4, 4);
INSERT INTO zelje(korisnik_id, termin_id) VALUES(4, 6);
INSERT INTO zelje(korisnik_id, termin_id) VALUES(4, 2);
INSERT INTO zelje(korisnik_id, termin_id) VALUES(4, 3);

CREATE TABLE kartice(
	id int PRIMARY KEY AUTO_INCREMENT,
    korisnik_id int,
    poeni int,
    popust int,
    odobrena boolean,
    FOREIGN KEY(korisnik_id) REFERENCES korisnici(id) ON DELETE cascade
);

INSERT INTO kartice(korisnik_id, poeni, popust, odobrena) VALUES(2, 12, poeni*5, true);
INSERT INTO kartice(korisnik_id, poeni, popust, odobrena) VALUES(4, 10, poeni*5, false);
select * from kartice;
-- CREATE TABLE komentari(
-- 	id int PRIMARY KEY AUTO_INCREMENT,
-- 	tekst text,
-- 	ocena int,
-- 	datum_postavljanja DATETIME,
-- 	autor_id int,
-- 	trening_id int,
-- 	status varchar(20),
-- 	anoniman boolean,
-- 	FOREIGN KEY(autor_id) REFERENCES korisnici(id) ON DELETE CASCADE,
-- 	FOREIGN KEY(trening_id) REFERENCES treninzi(id) ON DELETE CASCADE
-- );

-- INSERT INTO komentari(tekst, ocena, datum_postavljanja, autor_id, trening_id, status, anoniman)
-- 	VALUES('Trening je odlican, Milica i Nenad su divni!!!', 5, '2021-12-29 18:15:00', 2, 2, 'ODOBREN', true);
-- INSERT INTO komentari(tekst, ocena, datum_postavljanja, autor_id, trening_id, status, anoniman)
-- 	VALUES('Trening je odlican', 5, '2021-12-30 18:15:00', 4, 2, 'ODOBREN', true);
-- INSERT INTO komentari(tekst, ocena, datum_postavljanja, autor_id, trening_id, status, anoniman)
-- 	VALUES('Odlicno osmisljeni treninzi sa ciljanim vezbama', 5, '2022-01-03 18:15:00', 4, 6, 'ODOBREN', false);
-- INSERT INTO komentari(tekst, ocena, datum_postavljanja, autor_id, trening_id, status, anoniman)
-- 	VALUES('odlican', 5, '2021-12-29 18:15:00', 5, 5, 'ODOBREN', true);


-- SELECT * FROM korisnici;
-- SELECT * FROM tip_treninga; 
-- SELECT * FROM treninzi;
-- SELECT * FROM trening_tip;
-- SELECT * FROM sale;
-- SELECT * FROM termini_odrzavanja_treninga;
-- SELECT * FROM komentari;
	
